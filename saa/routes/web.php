<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::middleware('auth')->group(function () {


    Route::middleware('passwordChanged')->group(function () {
        //Route vers le front
        Route::prefix('front')->namespace('Front')->group(function () {

            //Route notifications
            Route::get('/notifications/unread','NotificationController@getUnreadNotificationOfUser');
            Route::get('/notifications/all', 'NotificationController@getAllNotification');
            Route::get('/notifications/markasread', 'NotificationController@markAsRead');
            Route::get('/notifications/delete', 'NotificationController@deleteNotification');
            Route::delete('/notifications/delete/{id}', 'NotificationController@deleteOneNotification');
            
            //Route offre
            //Route accessible que par les ancien eleve - responsable ou admin du forum ou admin
            Route::middleware('accessFrontCreateOffre')->group(function () {
                route::get('/frontOffre/create', 'OffreController@create')->name('front.offre.create');
                route::post('/frontOffre', 'OffreController@store')->name('front.offre.store');
            });
            //Route accessible que par l'utilisateur de l'offre ou admin
            Route::middleware('accessFrontEditOffre')->group(function () {
                route::patch('/frontOffre/{offre}', 'OffreController@update')->name('front.offre.update');
                route::get('/frontOffre/{offre}/edit', 'OffreController@edit')->name('front.offre.edit');
                route::delete('/frontOffre/{offre}', 'OffreController@destroy')->name('front.offre.destroy');
            });
            //Route accessible par tous
            route::get('/frontOffre', 'OffreController@index')->name('front.offre.index');       
            route::get('/frontOffre/{offre}', 'OffreController@show')->name('front.offre.show');        
            Route::any('/frontOffre/search', 'OffreController@search')->name('front.offre.search');
            Route::get('/frontForum/addFavoriteOffre/{offre}', 'OffreController@addFavorite')->name('front.offre.addFavorite');
            Route::get('/frontForum/removeFavoriteOffre/{offre}', 'OffreController@removeFavorite')->name('front.offre.removeFavorite');


            //Route forum
            //Route accessible que par l'utilisateur du forum ou admin 
            Route::middleware('accessFrontEditForum')->group(function () {
                route::patch('/frontForum/{topic}', 'ForumController@update')->name('front.forum.update');
                route::get('/frontForum/{topic}/edit', 'ForumController@edit')->name('front.forum.edit');
                route::delete('/frontForum/{topic}', 'ForumController@destroy')->name('front.forum.destroy');
            });
            //Route accessible par tous
            route::get('/frontForum', 'ForumController@index')->name('front.forum.index');
            route::get('/frontForum/create', 'ForumController@create')->name('front.forum.create');
            route::post('/frontForum', 'ForumController@store')->name('front.forum.store');
            route::get('/frontForum/{topic}', 'ForumController@show')->name('front.forum.show');
            Route::post('/frontForum/resolu/{topic}', 'ForumController@resoluForum')->name('front.forum.resolu');
            Route::get('/frontForum/addFavoriteTopic/{topic}', 'ForumController@addFavorite')->name('front.forum.addFavorite');
            Route::get('/frontForum/removeFavoriteTopic/{topic}', 'ForumController@removeFavorite')->name('front.forum.removeFavorite');
            Route::get('/mesForums', 'ForumController@indexMesForums')->name('front.mesForum.index');
            Route::any('/mesForums/search', 'ForumController@searchMesForums')->name('front.mesForum.search');


            //Route categorie
            //Route accessible par tous
            route::get('/frontCaregorie', 'CategorieController@index')->name('front.categorie.index');
            route::get('/frontCaregorie/{categorie}', 'CategorieController@show')->name('front.categorie.show');
            Route::any('/frontCaregorie/search/{categorie}', 'CategorieController@search')->name('front.categorie.search');


            //Route reponses forums
            //Route accessible par tous
            Route::post('/reponseTopic/{topic}', 'ReponseController@store')->name('front.reponse.store');
            Route::patch('/reponseTopic/{reponseTopic}', 'ReponseController@update')->name('front.reponse.update');
            Route::delete('/reponseTopic/{reponseTopic}', 'ReponseController@destroy')->name('front.reponse.destroy');


            //Route questionnaire
            Route::middleware('accessFrontQuestionnaire')->group(function () {
                Route::get('/questionnaire', 'QuestionnaireController@index')->name('front.questionnaire.index');
                Route::get('/questionnaire/{questionnaire}', 'QuestionnaireController@show')->name('front.questionnaire.show');
                Route::post('/questionnaire/reponseStore/{questionnaire}', 'QuestionnaireController@reponseStore')->name('front.questionnaire.reponseStore');
                Route::any('/questionnaire/search', 'QuestionnaireController@search')->name('front.questionnaire.search');

                Route::middleware('responsableQuestionnaire')->group(function () {
                    Route::get('/questionnaire/responsableQuestionnaire/create', 'QuestionnaireController@create')->name('front.questionnaire.create');
                    Route::post('/questionnaire/responsableQuestionnaire/store', 'QuestionnaireController@store')->name('front.questionnaire.store');
                    Route::get('/questionnaire/responsableQuestionnaire/{questionnaire}/edit', 'QuestionnaireController@edit')->name('front.questionnaire.edit');
                    route::patch('/questionnaire/responsableQuestionnaire/{questionnaire}', 'QuestionnaireController@update')->name('front.questionnaire.update');
                
                    Route::get('/questionnaire/{questionnaire}/{utilisateur}', 'QuestionnaireController@showUser')->name('front.questionnaire.showUser');
                    
                    //Route pdf
                    Route::get('/dynamic_pdf/pdf/{questionnaire}/{utilisateur}', 'QuestionnaireController@pdf')->name('front.questionnaire.pdf');
                    Route::get('/dynamic_pdf/allpdf/{questionnaire}/', 'QuestionnaireController@allpdf')->name('front.questionnaire.allpdf');
                });    
            });

            //Route profil
            Route::get('/profil', 'ProfilController@profil')->name('front.profil.index');
            Route::get('/favoris', 'ProfilController@favoris')->name('front.profil.favoris');
            Route::put('/profilUpdate', 'ProfilController@updateProfil')->name('front.profil.update');
            Route::put('/profilDesactive', 'ProfilController@desactiveAccount')->name('front.profil.desactive');
            Route::delete('/profilDelete', 'ProfilController@deleteAccount')->name('front.profil.delete');
        });

        //Route vers le back
        Route::middleware('admin')->group(function () {
            Route::prefix('admin')->namespace('Admin')->group(function () {
                Route::name('admin')->get('/', 'AdminController@index');
                Route::resource('/offre', 'OffreController');
                Route::resource('/forum', 'ForumController');
                Route::resource('/questionnaire', 'QuestionnaireController');
                Route::resource('/user', 'UserController');
                Route::resource('/categorie', 'CategorieController');
                Route::resource('/role', 'RoleController');
                Route::patch('/user/active/{user}', 'UserController@active')->name('user.active');
                Route::get('/questionnaire/{questionnaire}/elements-questionnaire/create', 'ElementQuestionnaireController@create')->name('elements-questionnaire.create');
                Route::post('/questionnaire/{questionnaire}/elements-questionnaire', 'ElementQuestionnaireController@store')->name('elements-questionnaire.store');
                route::patch('/questionnaire/{questionnaire}/elements-questionnaire/{element}', 'ElementQuestionnaireController@update')->name('elements-questionnaire.update');
                route::get('/questionnaire/{questionnaire}/elements-questionnaire/{element}/edit', 'ElementQuestionnaireController@edit')->name('elements-questionnaire.edit');
                route::delete('/questionnaire/{questionnaire}/elements-questionnaire/{element}', 'ElementQuestionnaireController@destroy')->name('elements-questionnaire.destroy');
            });
        });
    });
    
    Route::get('/home', 'HomeController@index')->name('home');
    
    Route::post('/changePassword', 'HomeController@changePassword')->name('change-password');
});

    Auth::routes();




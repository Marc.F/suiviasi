<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {
    Route::apiResource('users', 'Api\UserApiController');
    Route::apiResource('offres', 'Api\OffreApiController');
    Route::apiResource('forums', 'Api\ForumApiController');
    Route::apiResource('categories', 'Api\CategoryApiController');
    Route::apiResource('ReponseTopic', 'Api\ReponseTopicApiConntroller');
    Route::post('verifyPassword', 'Api\AuthMobileController@verifyPassword');
    Route::get('categories/forums/{Categorie}', 'Api\CategoryApiController@getForumsByCategoryId');
    Route::post('categories/lastForum', 'Api\CategoryApiController@getLastForumsByCategoryId');
    Route::get('forums/responses/{Topic}', 'Api\ForumApiController@getResponses');
    Route::get('forums/all/{Topic}', 'Api\ForumApiController@getAllFromForums');
    Route::post('forums/resolve', 'Api\ForumApiController@setResolveForum');
    Route::post('forums/checkFav', 'Api\ForumApiController@checkFavForum');
    Route::post('forums/setFav', 'Api\ForumApiController@setFavForum');
    Route::get('users/infos/{User}', 'Api\UserApiController@getUserInfos');
    Route::post('offres/setFav', 'Api\OffreApiController@setFavOffre');
});

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class AccessFrontEditOffre
{
    protected $auth;
    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth) {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {
        //SI cest son offre ou si il est admin alors ...
        $offreUserId = $request->route('offre')->utilisateur_id;
        if(auth()->user()->id_utilisateur == $offreUserId || auth()->user()->role->designation == "admin"){
            return $next($request);
        }
        return redirect()->route('home');
     }
}

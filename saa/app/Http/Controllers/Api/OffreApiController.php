<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Offre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OffreApiController extends Controller
{

    public function index()
    {
        return Offre::with('favorisOffre')->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Offre::with('favorisOffre')->where('id_offre', $id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function setFavOffre(Request $request)
    {
        $offre_id = $request->input('offre_id');
        $utilisateur_id = $request->input('utilisateur_id');

        $user = User::find($utilisateur_id);
        $offre = Offre::find($offre_id);

        if($user->favorisOffre->contains($offre))
        {
            $user->favorisOffre()->detach($offre_id);
            return response()->json([
                'fav' => false,
                ]);
        }
        else
        {
            $user->favorisOffre()->attach($offre_id);
            return response()->json([
                'fav' => true,
                ]);
        }

        return response()->json(['fav' => null]);
    }
}

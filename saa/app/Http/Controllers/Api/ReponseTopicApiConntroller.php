<?php

namespace App\Http\Controllers\Api;

use App\Models\Topic;
use App\Models\ReponseTopic;
use Illuminate\Http\Request;
use App\Notifications\NewResponse;
use App\Http\Controllers\Controller;

class ReponseTopicApiConntroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $topic = Topic::find($request->input('topic_id'));

        $reponse = new ReponseTopic;
        $reponse->topic_id = $request->input('topic_id');
        $reponse->utilisateur_id = $request->input('utilisateur_id');
        $reponse->description = $request->input('reponse');
        $reponse->save();

          // Création des différente notification
          if($topic->user->id_utilisateur != $reponse->utilisateur_id)
          {
              $topic->user->notify(new NewResponse($reponse));
          }
  
          foreach($topic->favorisTopic as $user){
              if($topic->user->id_utilisateur != $user->id_utilisateur && $reponse->utilisateur_id != $user->id_utilisateur)
              {
                  $user->notify(new NewResponse($reponse));
              }
              
          }

        return response()->json(['create' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getReponseByForumId(Request $request)
    {
        $id = $request->input('id_forum');

        $forum = Topic::find($id);

        if($forum != null)
        {
            return $forum->reponsesTopic();
        }
        return response()->json(['error' => true]);
    }
}

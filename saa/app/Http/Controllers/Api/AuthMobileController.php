<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class AuthMobileController extends Controller
{
    public function verifyPassword(Request $request)
    {
        $id = $request->input('id_utilisateur');
        $password = $request->input('password');

        $user = User::find($id);

        if(Hash::check($password, $user->password))
        {
            return response()->json(['login' => true]);
        }
        return response()->json(['login' => false]);
    }
}

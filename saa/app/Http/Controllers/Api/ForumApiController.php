<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ForumApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Topic::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Topic::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getResponses($id)
    {
        $topic = Topic::Find($id);

        if($topic != null)
        {
            return $topic->reponsesTopic;
        }
    }

    public function setResolveForum(Request $request)
    {
        $id = $request->input('topic_id');

        $forum = Topic::find($id);

        $forum->cloturer = $request->input('cloturer');
        $forum->save();

        return response()->json(['cloturer' => true]);
    }

    public function checkFavForum(Request $request)
    {
        $topic_id = $request->input('topic_id');
        $utilisateur_id = $request->input('utilisateur_id');

        $user = User::Find($utilisateur_id);
        $topic = Topic::find($topic_id);

        if($user->favorisTopic->contains($topic))
        {
            return response()->json(['fav' => true]);
        }
        return response()->json(['fav' => false]);
    }

    public function setFavForum(Request $request)
    {
        $topic_id = $request->input('topic_id');
        $utilisateur_id = $request->input('utilisateur_id');

        $user = User::Find($utilisateur_id);
        $topic = Topic::find($topic_id);

        if($user->favorisTopic->contains($topic))
        {
            $user->favorisTopic()->detach($topic_id);
            return response()->json(['fav' => false]);
        }
        else
        {
            $user->favorisTopic()->attach($topic_id);
            return response()->json(['fav' => true]);
        }

        return response()->json(['fav' => null]);

    }

    public function getAllFromForums($id)
    {
        $topic = Topic::With('user')->With('reponsesTopic')->with('reponsesTopic.user')->with('favorisTopic')->where('id_topic', $id)->first();

        return $topic;
    }
}

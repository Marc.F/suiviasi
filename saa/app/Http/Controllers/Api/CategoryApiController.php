<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Categorie;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cateogies = Categorie::With('topics')->With('topics.user')->get();

        return $cateogies;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getForumsByCategoryId($id)
    {
        $cateogie = Categorie::with('topics')->with('topics.favorisTopic')->where('id_categorie', $id)->first();

        if($cateogie != null)
        {
            return response()->json([
                'forumList' => $cateogie->topics,
            ]);
        }
        return response()->json(['error' => true]);
    }

    public function getLastForumsByCategoryId(Request $request)
    {
        $id = $request->input('id_categorie');

        $cateogie = Categorie::find($id);

        if($cateogie != null)
        {
            return response()->json([
                'lastTopic' => $cateogie->topics->sortBy('created_at')->last(),
                'lastTopicUser' => User::find($cateogie->topics->sortBy('created_at')->last()->utilisateur_id)
            ]);
        }
        return response()->json(['error' => true]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Questionnaire;
use App\Models\ElementQuestionnaire;
use App\Models\Role;
use App\Notifications\NewQuestionnaire;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.questionnaire.questionnaire', ['questionnaires' => Questionnaire::all()]);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.questionnaire.createQuestionnaire');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $questionnaire = new Questionnaire();
        $questionnaire->titre = $request->get('titre');
        $questionnaire->save();

        $role = Role::where('designation', "ancien_etudiant")->first();

        foreach($role->users as $user)
        {
            $user->notify(new NewQuestionnaire($questionnaire));
        }

        return view('admin.questionnaire.questionnaire', ['questionnaires' => Questionnaire::all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {
        $elements = Questionnaire::find($questionnaire->id_questionnaire)->elements;
        return view ('admin.questionnaire.showQuestionnaire', ['questionnaire' => $questionnaire, 'elements' => $elements]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Questionnaire $questionnaire)
    {
        return view('admin.questionnaire.editQuestionnaire' , ['questionnaire' => $questionnaire, 'questionnaires' => Questionnaire::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questionnaire $questionnaire)
    {
        $this->validateData($request);
        $this->save($questionnaire, $request);

        return redirect()->route('questionnaire.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionnaire $questionnaire)
    {
        $elements = Questionnaire::find($questionnaire->id_questionnaire)->elements;
        foreach ($elements as $element) {
            $element->delete();
        }
        $questionnaire->delete();
        return redirect()->route('questionnaire.index');
    }

    private function save(Questionnaire $questionnaire, Request $request)
    {
        $questionnaire->titre = $request->titre;
        $questionnaire->save();
    }
    private function validateData(Request $request)
    {
        return $request->validate([
            'titre' => 'required|string|max:255',
        ]);
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Topic;
use App\Models\Categorie;
use Illuminate\Http\Response;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.forum.indexForum', ['forums' => Topic::All()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view ('admin.forum.createForum', ['categories' => Categorie::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validateData($request);
        
        $topic = new Topic();
        $this->save($topic, $request, 0);

        return redirect()->route('forum.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Topic $forum)
    {
        return view ('admin.forum.editForum', ['topic' => $forum, 'categories' => Categorie::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, Topic $forum)
    {
        $this->validateData($request);
        
        $this->save($forum, $request, 1);

        return redirect()->route('forum.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Topic $forum)
    {
        $forum->delete();
  
        return redirect()->route('forum.index')->withToastSuccess('Forum supprimé avec succès !!');

    }

    private function save(Topic $topic, Request $request, $int)
    {
        $topic->titre = $request->titre;
        $topic->categorie_id = $request->categorie_id;
        $topic->description = $request->description;
        if($int == 0){
            $topic->cloturer = 0;
        }else{
            if(! $request->has('cloturer'))
                $topic->cloturer = 0;
            else
                $topic->cloturer = 1;
        }
        $topic->save();
    }

    private function validateData(Request $request)
    {
        return \Validator::make($request->all(),[
            'titre' => 'required|string|max:255',
            'categorie_id' => 'required|integer',
            'description' => 'required|string|min:3',
        ])->validate();
    }
}

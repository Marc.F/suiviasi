<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Questionnaire;
use App\Models\ElementQuestionnaire;

class ElementQuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Questionnaire $questionnaire)
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Questionnaire $questionnaire_id
     * @return \Illuminate\Http\Response
     */

    public function create(Questionnaire $questionnaire)
    {
        return view('admin.questionnaire.element_questionnaire.createElement', ['questionnaire' => $questionnaire]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Questionnaire $questionnaire)
    {
        $element = new ElementQuestionnaire();
        $element->description = $request->get('description');
        $element->questionnaire_id = $questionnaire->id_questionnaire;
        $element->save();
        return redirect()->route('questionnaire.show', ['questionnaire' => $questionnaire]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Questionnaire $questionnaire, ElementQuestionnaire $element)
    {
        return view('admin.questionnaire.element_questionnaire.editElement' , ['questionnaire' => $questionnaire, 'element' => $element]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questionnaire $questionnaire, ElementQuestionnaire $element)
    {
        $this->validateData($request);
        $this->save($element, $request);

        return redirect()->route('questionnaire.show', ['questionnaire' => $questionnaire]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Questionnaire $questionnaire, ElementQuestionnaire $element)
    {
        $element->delete();
        return redirect()->route('questionnaire.show', ['questionnaire' => $questionnaire]);
    }

    private function save(ElementQuestionnaire $element, Request $request)
    {
        $element->description = $request->description;
        $element->save();
    }
    private function validateData(Request $request)
    {
        return \Validator::make($request->all(),[
            'description' => 'required|string|max:255',
        ])->validate();
    }
}

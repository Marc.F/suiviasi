<?php

namespace App\Http\Controllers\Admin;

use App\Models\Categorie;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.categorie.indexCategorie', ['categories' => Categorie::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categorie.createCategorie');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateData($request, null);

        $categorie = new Categorie();

        $this->save($categorie, $request);

        return redirect()->route('categorie.index')->withToastSuccess('Categorie créer avec succès !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Categorie $categorie)
    {
        return view ('admin.categorie.editCategorie', ['categorie' => $categorie]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categorie $categorie)
    {
        $this->validateData($request, $categorie);

        $this->save($categorie, $request);

        return redirect()->route('categorie.index')->withToastSuccess('Catégorie edité avec succès !!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categorie $categorie)
    {
        if($categorie->delete())
        {
            return redirect()->route('categorie.index')->withToastSuccess('Catégorie supprimé avec succès !!');
        }else
        return redirect()->route('categorie.index')->withToastError('Erreur lors de la suppression de la categorie !!');
    }

    private function save(Categorie $categorie, Request $request)
    {
        $categorie->titre_categorie = $request->titre_categorie;
        $categorie->description_categorie = $request->description_categorie;
        $categorie->save();
        $this->storeFile($categorie);
    }

    private function validateData(Request $request, $categorie)
    {
        if($categorie != null)
        {
            return Validator::make($request->all(),[
                'titre_categorie' => 'required|string|max:255|'.Rule::unique('categories')->ignore($categorie->id_categorie, 'id_categorie'),
                'description_categorie' => 'required|string|max:255',
                'logo_categorie' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ])->validate();
        }
        else
        {
            return Validator::make($request->all(),[
                'titre_categorie' => 'required|string|max:255|',
                'description_categorie' => 'required|string|max:255',
                'logo_categorie' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ])->validate();
        }
    }

    private function storeFile(Categorie $categorie)
    {
        if(request('logo_categorie'))
        {
            $categorie->update([
                'logo_categorie' => request()->file('logo_categorie')->store('categorieLogo')
            ]);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Offre;

class OffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.offre.indexOffre', ['offres' => Offre::All()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('admin.offre.createOffre', ['offres' => Offre::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $offre = new Offre();
        $this->save($offre, $request);
        
        return redirect()->route('offre.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Offre $offre)
    {
        return view ('admin.offre.showOffre', ['offre' => $offre, 'offres' => Offre::all()]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Offre $offre)
    {
        return view ('admin.offre.editOffre', ['offre' => $offre, 'offres' => Offre::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offre $offre)
    {
        $this->validateData($request);

        $this->save($offre, $request);

        return redirect()->route('offre.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offre $offre)
    {
        $offre->delete();

        return redirect()->route('offre.index')->withToastSuccess('Offre supprimé avec succès !!');
    }

    private function save(Offre $offre, Request $request)
    {
        $offre->titre = $request->titre;
        $offre->description = $request->description;
        $offre->adresse = $request->adresse;
        $offre->ville = $request->ville;
        $offre->code_postal = $request->code_postal;
        $offre->mail = $request->mail;
        $offre->telephone = $request->telephone;
        $offre->entreprise = $request->entreprise;
        $offre->salaire = $request->salaire;
        $offre->niveau_etude = $request->niveau_etude;
        $offre->save();
    }

    private function validateData(Request $request)
    {
        return \Validator::make($request->all(),[
            'titre' => 'required|string|max:255',
            'description' => 'required|string|min:3',
            'adresse' => 'required|string|max:255',
            'ville' => 'required|string|max:255',
            'code_postal' => 'required|string|max:255',
            'mail' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'entreprise' => 'required|string|max:255',
            'salaire' => 'required|string|max:255',
            'niveau_etude' => 'required|string',
        ])->validate();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('admin.user.indexUser', ['users' => User::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.user.createUser', ['roles' => Role::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $this->validateData($request, null);

        $user = new User();
        $user->password_changed = 0;
        $this->save($user, $request);
        $this->sendEmail($request->email, $request->password);

        return redirect()->route('user.index')->withToastSuccess('Utilisateur créer avec succès !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(User $user)
    {
        return view('admin.user.editUser', ['user' => $user, 'roles' => Role::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, User $user)
    {
        $this->validateData($request, $user);
        
        $user->password_changed = 0;
        $this->save($user, $request);

        return redirect()->route('user.index')->withToastSuccess('Utilisateur modifier avec succès !!');;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Response
     * @throws Exception
     */
    public function destroy(User $user)
    {
        if($user->delete())
        {
            return redirect()->route('user.index')->withToastSuccess('Utilisateur supprimé avec succès !!');
        }
        return redirect()->route('user.index')->withToastError('Erreur lors de la suppression de l\' utilisateur !!');

    }

    public function active(Request $request, User $user)
    {
        $user->active = !$user->active;
        $user->save();

        return redirect()->back()->withToastSuccess('l\'utilisateur à changer de status');
    }

    private function save(User $user, Request $request)
    {
        $user->nom = $request->nom;
        $user->prenom = $request->prenom;
        $user->email = $request->email;
        $user->telephone = $request->telephone;
        $user->role_id = $request->role_id;
        $user->password = Hash::make($request->password);
        $user->active = true;
        $user->save();
    }

    private function validateData(Request $request, $user)
    {
        if($user != null)
        {
            return \Validator::make($request->all(),[
                'nom' => 'required|string|max:255',
                'prenom' => 'required|string|max:255',
                'telephone' => 'required|string|max:255',
                'email' => 'required|email|'.Rule::unique('users')->ignore($user->id_utilisateur, 'id_utilisateur').'|max:255',
                'role_id' => 'required|integer',
                'password' => 'required|string|min:8|confirmed|max:255',
            ])->validate();
        }
        else
        {
            return \Validator::make($request->all(),[
                'nom' => 'required|string|max:255',
                'prenom' => 'required|string|max:255',
                'telephone' => 'required|string|max:255',
                'email' => 'required|email|max:255|unique:users',
                'role_id' => 'required|integer',
                'password' => 'required|string|min:8|confirmed|max:255',
            ])->validate();
        }
    }

    private function sendEmail($mail, $password){
        $data = array('email' => $mail, 'password' => $password);

        Mail::send('mail', $data, function($message) use ($mail){
            $message->to($mail, 'SuiviASI')->subject('Inscription à la plateforme SuiviASI');
        });
    }
}

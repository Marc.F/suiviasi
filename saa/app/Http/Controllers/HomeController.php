<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offre;
use App\Models\Topic;
use App\Models\Questionnaire;
use App\Models\ElementQuestionnaire;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->password_changed)
        {
            $lesOffres = Offre::latest()->take(3)->get();
            $lesTopics = Topic::latest()->take(3)->get();
            $lesQuestionnaires = Questionnaire::latest()->take(3)->get();
            return view('home', ['lesOffres' => $lesOffres, 'lesTopics' => $lesTopics, 'lesQuestionnaires' => $lesQuestionnaires]);
        }
        else
        {
            return view('auth.passwords.changePassword');
        }
    }

    public function changePassword(Request $request)
    {
        $request->validate([
            'password' => 'required|min:8|max:50|confirmed',
        ]);

        $user = Auth::user();
        
        $user->password = Hash::make($request->password);
        $user->password_changed = true;
        $user->save();
        
        return redirect()->route('home');
    }
}

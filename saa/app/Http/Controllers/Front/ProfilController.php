<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Hash;


class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function profil()
    {
        return view ('front.profil.indexProfil');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function favoris()
    {
        $lesTopics = auth()->user()->favorisTopic->paginate(3, 'lesTopics');
        $lesOffres = auth()->user()->favorisOffre->paginate(3, 'lesOffres');

        return view ('front.profil.indexFavoris', ['lesTopics' => $lesTopics, 'lesOffres' => $lesOffres]);
    }

    public function updateProfil(Request $request)
    {   
        $user = Auth::user();
        $all = User::all();

        if(Hash::check($request->input('password'), $user->password)){ //Verification du mdp de passe du compte
            if($request->has('infoBox')){ // si la box est coché
                
                $validator = \Validator::make($request->all(), [
                    'nom' => 'required',
                    'prenom' => 'required',
                    'email' => 'required|email',
                    'telephone' => 'required|digits:10',
                    'password' => 'required',
                ])->validate();
                
                foreach($all as $oneUser){ // On parcours les utilisateurs 
                    if($oneUser->email == $request->input('email')){ // Si l'adresse mail entré est déjà existante alors on retourne l'érreur
                        if($oneUser != $user){//Si ce n'est pas l'utilisateur connecter
                            return redirect()->back()->withErrors("Adresse mail déja existante")->withInput();
                        }
                    }
                }

                $user->nom = $request->input('nom');
                $user->prenom = $request->input('prenom');
                $user->email = $request->input('email');
                $user->telephone = $request->input('telephone');
                $user->save();
                return redirect()->back()->withSuccess("Informations personnelles mises à jour !");

            }else if($request->has('mdpBox')){ // sinon si la mdp box est coché

                if($request->input('newPassword') != $request->input('newPasswordConfirmation')){ // Si les deux Nouveau mdp ne sont pas identiques
                    return redirect()->back()->withErrors("Les deux nouveau mots de passe ne sont pas identiques !")->withInput();
                }else if($request->input('password') == $request->input('newPassword')){ // Si le Nouveau mdp est identique à l'ancien on indique l'erreur
                    return redirect()->back()->withErrors("Mot de passe identique à l'ancien !")->withInput();
                }else{  // Sinon on fait le changement
                    $user->password = bcrypt($request->input('newPassword'));
                    $user->save();
                    return redirect()->back()->withSuccess("Mot de passe changé !");
                }
            }
        }else{
            return redirect()->back()->withErrors("Mauvais mot de passe !")->withInput();
        }
    }

    public function desactiveAccount(Request $request)
    {
        $user = Auth::user();
        $user->active = !$user->active;
        $user->save();

        Auth::logout();
        return redirect()->route('login');
    }

    public function deleteAccount(Request $request)
    {
        $user = Auth::user();
        $user->nom = "supprimé";
        $user->prenom = "utilisateur";
        $user->telephone = "0000000000";
        $user->email = "utilisateursupprime@gmail.com";
        $user->password = "";
        $user->active = 0;
        $user->save();
        
        Auth::logout();
        return redirect()->route('login');
    }
}

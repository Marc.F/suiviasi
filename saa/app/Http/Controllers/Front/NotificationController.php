<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\NewResponse;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;

class NotificationController extends Controller
{
    public function getUnreadNotificationOfUser(){
        return response()->json(count(auth()->user()->unreadNotifications));
    }

    public function getAllNotification(){
        return response()->json(auth()->user()->notifications);
    }

    public function markAsRead()
    {
        auth()->user()->unreadNotifications->markAsRead();
    }

    public function deleteNotification()
    {
        $user = User::find(auth()->id());

        $user->notifications()->delete();
    }

    public function deleteOneNotification($id)
    {
        $notif = auth()->user()->notifications->where('id',$id)->first();
        
        $notif->delete();
    }

}

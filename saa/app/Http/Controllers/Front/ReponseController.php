<?php

namespace App\Http\Controllers\Front;
use App\Models\Topic;

use App\Models\ReponseTopic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Notifications\NewResponse;

class ReponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Topic $topic)
    {
        //Validation de la réponse
        $request->validate([
            'reponse' => 'required|min:3',
          ]);

          //Création de la réponse en BDD
        $reponse = new ReponseTopic;
        $reponse->topic_id = $topic->id_topic;
        $reponse->description =$request->input('reponse');
        $reponse->utilisateur_id = Auth::id();
        $reponse->save();

        // Création des différente notification
        if($topic->user->id_utilisateur != Auth::id())
        {
            $topic->user->notify(new NewResponse($reponse));
        }

        foreach($topic->favorisTopic as $user){
            if($topic->user->id_utilisateur != $user->id_utilisateur && $reponse->utilisateur_id != $user->id_utilisateur)
            {
                $user->notify(new NewResponse($reponse));
            }
            
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReponseTopic  $reponseTopic
     * @return \Illuminate\Http\Response
     */
    public function show(ReponseTopic $reponseTopic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReponseTopic  $reponseTopic
     * @return \Illuminate\Http\Response
     */
    public function edit(ReponseTopic $reponseTopic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReponseTopic  $reponseTopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReponseTopic $reponseTopic)
    {
        $request->validate([
            'modifReponse' => 'required|min:3',
        ]);

        $reponseTopic->description =$request->input('modifReponse');
        $reponseTopic->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReponseTopic  $reponseTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $reponseTopic)
    {
        $reponse = ReponseTopic::find($reponseTopic);
        $reponse->delete();
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers\front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Questionnaire;
use App\Models\reponseStore;
use App\Models\ElementQuestionnaire;
use App\Models\ReponseElement;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Models\User;
use App\Models\Role;
use App\Notifications\NewResponseQuestionnaire;
use PDF;
use ZipArchive;
use File;
use Illuminate\Support\Facades\Storage;
use App\Notifications\NewQuestionnaire;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role->designation == "ancien_etudiant")
        {
            $lesQuestionnaires = Questionnaire::orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->get();
            
            $variableTampon = $this->classerQuestionnaire($lesQuestionnaires);

            $lesQuestionnaires = $this->paginate($variableTampon[0],9);
            $lesRepondu = $variableTampon[1];

            return view ('front.questionnaire.indexQuestionnaire', ["lesQuestionnaires" => $lesQuestionnaires, "lesRepondu" => $lesRepondu]);
        }else
        {
            $lesQuestionnaires = Questionnaire::orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->paginate(9);
            $lesUsers = User::where('role_id', Role::where('designation', 'responsable')->first()->id_role)->get();
            
            return view ('front.questionnaire.viewResponsable.indexQuestionnaire')->with('lesQuestionnaires', $lesQuestionnaires);
        }
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('front.questionnaire.viewResponsable.createQuestionnaire');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titre' => 'required|string|max:255',
        ]);

        $questionnaire = new Questionnaire();
        $questionnaire->titre = $request->get('titre');
        $questionnaire->save();

        for($i = 1; $i>0; $i++){
            $text = "question$i";
            if($request->$text == null){
                break;
            }
            $element = new ElementQuestionnaire();
            $element->description = $request->get($text);
            $element->questionnaire_id = $questionnaire->id_questionnaire;
            $element->save();
        }

        $role = Role::where('designation', "ancien_etudiant")->first();

        foreach($role->users as $user)
        {
            $user->notify(new NewQuestionnaire($questionnaire));
        }
        
        $lesQuestionnaires = Questionnaire::orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->paginate(9);
            
        return view ('front.questionnaire.viewResponsable.indexQuestionnaire')->with('lesQuestionnaires', $lesQuestionnaires);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Questionnaire $questionnaire)
    {
        return view('front.questionnaire.viewResponsable.editQuestionnaire' , ['questionnaire' => $questionnaire]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Questionnaire $questionnaire)
    {
        $request->validate([
            'titre' => 'required|string|max:255',
        ]);
        $questionnaire->titre = $request->titre;
        $questionnaire->save();

        $lesElements = ElementQuestionnaire::where('questionnaire_id', $questionnaire->id_questionnaire)->get();
        $count = 1;
        foreach($lesElements as $element){  
            $text = "question_$count";
            $request->validate([
                $text => 'required|max:255',
            ]);
            $count++;
        }

        $count = 1;
        foreach($lesElements as $element){
            $text = "question_$count";
            $element->description = $request->$text;
            $element->save();
            $count++;
        }

        $lesQuestionnaires = Questionnaire::orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->paginate(9);
            
        return view ('front.questionnaire.viewResponsable.indexQuestionnaire')->with('lesQuestionnaires', $lesQuestionnaires);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showUser(Questionnaire $questionnaire, User $utilisateur)
    {
        $lesElements = ElementQuestionnaire::where('questionnaire_id', $questionnaire->id_questionnaire)->get();
        $reponseElements = array();
        foreach($lesElements as $element){ 
            array_push($reponseElements, ReponseElement::where('element_id', $element->id_element)->where('utilisateur_id', $utilisateur->id_utilisateur)->first());
        }
 
        return view ('front.questionnaire.viewResponsable.showReponseUserQuestionnaire', ["questionnaire" => $questionnaire, 'reponseElements' => $reponseElements, 'utilisateur' => $utilisateur]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Questionnaire $questionnaire)
    {
        if(auth()->user()->role->designation == "admin" || auth()->user()->role->designation == "responsable")
        {
            $lesElements = ElementQuestionnaire::where('questionnaire_id', $questionnaire->id_questionnaire)->get();

            $roleAncien = Role::where('designation', 'ancien_etudiant')->first();
            $lesAncienEtudiant = User::where('role_id', $roleAncien->id_role)->get();

            $userRepondu = array();

            foreach($lesAncienEtudiant as $ancienEtudiant){
                $present = 0;
                foreach($lesElements as $element){             
                    if(count(ReponseElement::where('utilisateur_id', $ancienEtudiant->id_utilisateur)->where('element_id', $element->id_element)->get()) != 0){
                        $present = 1;
                        break;
                    }
                }
                if($present == 1){
                    array_push($userRepondu, $ancienEtudiant);
                }
            }

            return view ('front.questionnaire.viewResponsable.showQuestionnaire', ["questionnaire" => $questionnaire, "utilisateurRepondu" => $userRepondu]);
        }else{
            $lesElements = ElementQuestionnaire::where('questionnaire_id', $questionnaire->id_questionnaire)->get();
            $reponseElements = array();
            foreach($lesElements as $element){ 
                array_push($reponseElements, ReponseElement::where('element_id', $element->id_element)->where('utilisateur_id', auth()->user()->id_utilisateur)->first());
            }
            return view ('front.questionnaire.showQuestionnaire', ["questionnaire" => $questionnaire, 'reponseElements' => $reponseElements]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reponseStore(Request $request, Questionnaire $questionnaire)
    {
        $lesElements = ElementQuestionnaire::where('questionnaire_id', $questionnaire->id_questionnaire)->get();
        foreach($lesElements as $element){  
            
            $request->validate([
                str_replace(" ","_",$element->description) => 'required|string|max:500',
            ]);
        }

        foreach($lesElements as $element){
            $id = str_replace(" ","_",$element->description);
            $reponseElement = new ReponseElement();
            $reponseElement->description = $request->$id;
            $reponseElement->element_id = $element->id_element;
            $reponseElement->utilisateur_id = auth()->user()->id_utilisateur;
            $reponseElement->save();
        }

        $role = Role::where("designation", "responsable")->first();

        foreach($role->users as $user)
        {
            $user->notify(new NewResponseQuestionnaire($questionnaire, auth()->user()));
        }

        return redirect()->route("front.questionnaire.index");
    }

    public function search(Request $request)
    {
        $search = $request->search;

        if(auth()->user()->role->designation == "ancien_etudiant")
        {
            $lesQuestionnaires = Questionnaire::where('titre', 'LIKE', "%".$search."%")->orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->get();
            
            $variableTampon = $this->classerQuestionnaire($lesQuestionnaires);

            $lesQuestionnaires = $this->paginate($variableTampon[0],2);
            $lesRepondu = $variableTampon[1];

            return view ('front.questionnaire.indexQuestionnaire', ["lesQuestionnaires" => $lesQuestionnaires, "lesRepondu" => $lesRepondu]);
        }else{
            $lesQuestionnaires = Questionnaire::where('titre', 'LIKE', "%".$search."%")->orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->paginate(9);
            $lesUsers = User::where('role_id', Role::where('designation', 'responsable')->first()->id_role)->get();
            
            //calculer cb dutilisateurs ont repondu au questionnaire
            
            return view ('front.questionnaire.viewResponsable.indexQuestionnaire')->with('lesQuestionnaires', $lesQuestionnaires);
        }
    }

    public function classerQuestionnaire($lesQuestionnaires)
    {
        $lesQuestionnairesClasser = array();
        $lesRepondu = array();
        foreach($lesQuestionnaires as $questionnaire){
            $lesElements = ElementQuestionnaire::where('questionnaire_id', $questionnaire->id_questionnaire)->get();
            foreach($lesElements as $element){
                if(ReponseElement::where('element_id', $element->id_element)->where('utilisateur_id', auth()->user()->id_utilisateur)->first() != null){
                    break;
                }else{
                    array_push($lesQuestionnairesClasser, $questionnaire);
                    $obj = (object) array('id_questionnaire' => $questionnaire->id_questionnaire);
                    array_push($lesRepondu, $obj);
                    break;
                }
            }
        }

        foreach($lesQuestionnaires as $questionnaire){
            $i = 0;
            $lesElements = ElementQuestionnaire::where('questionnaire_id', $questionnaire->id_questionnaire)->get();
            if($lesElements->count() != 0){
                foreach($lesQuestionnairesClasser as $questionnaireClasser){
                    if($questionnaireClasser->id_questionnaire == $questionnaire->id_questionnaire){
                        $i++;
                        break;
                    }
                }
                if($i == 0){
                    array_push($lesQuestionnairesClasser, $questionnaire);
                }
            }
        }
        
        $array = array();

        array_push($array, $lesQuestionnairesClasser);
        array_push($array, $lesRepondu);
        
        return $array;
    }

    public function paginate($items, $perPage = 2, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator(
            $items->forPage($page, $perPage)->values(),
            $items->count(),
            $perPage,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );
    }

    public function pdf(Questionnaire $questionnaire, User $utilisateur)
    {
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($this->convert_customer_data_to_html($questionnaire, $utilisateur));
        return $pdf->download('document.pdf');
    }

    function allpdf(Questionnaire $questionnaire)
    {
        $lesElements = ElementQuestionnaire::where('questionnaire_id', $questionnaire->id_questionnaire)->get();

        $roleAncien = Role::where('designation', 'ancien_etudiant')->first();
        $lesAncienEtudiant = User::where('role_id', $roleAncien->id_role)->get();

        $userRepondu = array();

        foreach($lesAncienEtudiant as $ancienEtudiant){
            $present = 0;
            foreach($lesElements as $element){             
                if(count(ReponseElement::where('utilisateur_id', $ancienEtudiant->id_utilisateur)->where('element_id', $element->id_element)->get()) != 0){
                    $present = 1;
                    break;
                }
            }
            if($present == 1){
                array_push($userRepondu, $ancienEtudiant);
            }
        }
        foreach($userRepondu as $utilisateur){
            $name = $utilisateur->nom . $utilisateur->prenom;
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($this->convert_customer_data_to_html($questionnaire, $utilisateur));
            Storage::put('pdf/'.$name.'.pdf', $pdf->output());
        }

        $zip = new ZipArchive;
        $fileName = 'myNewFile.zip';
        if ($zip->open(public_path('storage/zip/').$fileName, ZipArchive::CREATE) === TRUE)
        {
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                $files = File::files(public_path('storage\pdf'));
            } else {
                $files = File::files(public_path('storage/pdf'));
            }
            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }
            $zip->close();
        }
        foreach($userRepondu as $utilisateur){
            $name = $utilisateur->nom . $utilisateur->prenom;
            Storage::delete('pdf/'.$name.'.pdf');
        }

        return response()->download(public_path('storage/zip/').$fileName)->deleteFileAfterSend(true);
    }

    function convert_customer_data_to_html(Questionnaire $questionnaire, User $utilisateur)
    {
        $lesElements = ElementQuestionnaire::where('questionnaire_id', $questionnaire->id_questionnaire)->get();
        $customer_data = array();
        foreach($lesElements as $element){ 
            array_push($customer_data, ReponseElement::where('element_id', $element->id_element)->where('utilisateur_id', $utilisateur->id_utilisateur)->first());
        }
        $output = '<h1 align="center">'.$questionnaire->titre.' ('.$utilisateur->nom.' '.$utilisateur->prenom.')</h1>';
        $count = 1;
        foreach($questionnaire->elements as $element)
        {
            $output .= '<h3>'.$count.'. '.$element->description.'</h3>';
            foreach($customer_data as $data){
                if($data->element_id == $element->id_element){
                    $output .= '<p style="margin-left: 1em;">'.$data->description.'</p>';
                }
            }
            $count++;
        }
        
        return $output;
    }

}

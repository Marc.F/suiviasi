<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categorie;
use App\Models\Topic;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lesCategories = Categorie::orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->paginate(12);
        return view ('front.categorie.indexCategorie')->with("lesCategories",$lesCategories);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Categorie $categorie)
    {
        $lesTopics = Topic::where('categorie_id', $categorie->id_categorie)->orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->latest()->paginate(10);
        return view ('front.categorie.showCategorie', ['lesTopics' => $lesTopics, 'categorie' => $categorie]);
    }

    public function search(Request $request, Categorie $categorie)
    {
        $search = $request->search;
        $lesTopics = Topic::where([['titre', 'LIKE', "%".$search."%"],['categorie_id', $categorie->id_categorie]])->orWhere([['description', 'LIKE', "%".$search."%"],['categorie_id', $categorie->id_categorie]])->orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->latest()->paginate(10);
        return view ('front.categorie.showCategorie', ['lesTopics' => $lesTopics, 'categorie' => $categorie]);
    }
}

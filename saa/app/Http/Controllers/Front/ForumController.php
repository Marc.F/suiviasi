<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Topic;
use App\Models\Categorie;
use App\Models\ReponseTopic;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Artisan;

class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Categorie::all();
        return view ('front.forum.createForum')->with('lesCategories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validateData($request);
        
        $topic = new Topic();
        $this->save($topic, $request);

        return redirect()->route('front.forum.show', [$topic]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Topic $topic)
    {
        $lesReponses = ReponseTopic::where('topic_id', $topic->id_topic)->paginate(9);
        return view ('front.forum.showForum', ['topic' => $topic, 'lesReponses' => $lesReponses]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Topic $topic)
    {
        return view ('front.forum.editForum', ['topic' => $topic, 'categories' => Categorie::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, Topic $topic)
    {
        $this->validateData($request);
        
        $this->save($topic, $request);

        return redirect()->route('front.forum.show', [$topic->id_topic]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Topic $topic)
    {
        $topic->delete();
        return redirect()->route('front.categorie.show', $topic->categorie);
    }

    //CheckBoxResolu
    public function resoluForum(Request $request, Topic $topic)
    {   
        if(isset($request->scales)){
            $topic->cloturer = 1;
            $topic->save();
        }else{
            $topic->cloturer = 0;
            $topic->save();
        }
        
        return redirect()->back();
    }

    public function indexMesForums()
    {
        $lesTopics = Topic::where('utilisateur_id', Auth::id())->paginate(9);
        return view('front.forum.indexMesForum')->with('lesTopics', $lesTopics);
    }

    public function searchMesForums(Request $request)
    {
        $search = $request->search;
        $lesTopics = Topic::where([['titre', 'LIKE', "%".$search."%"],['utilisateur_id', Auth::id()]])->orWhere([['description', 'LIKE', "%".$search."%"],['utilisateur_id', Auth::id()]])->orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->latest()->paginate(10);
        return view ('front.forum.indexMesForum', ['lesTopics' => $lesTopics]);
    }

    public function addFavorite(Topic $topic)
    {
        auth()->user()->favorisTopic()->attach($topic->id_topic);
        return redirect()->route('front.forum.show', $topic);
    }

    public function removeFavorite(Topic $topic)
    {
        auth()->user()->favorisTopic()->detach($topic->id_topic);
        return redirect()->route('front.forum.show', $topic);
    }


    private function save(Topic $topic, Request $request)
    {
        $topic->titre = $request->titre;
        $topic->categorie_id = $request->categorie_id;
        $topic->description = $request->description;
        $topic->cloturer = 0;
        $topic->save();
    }

    private function validateData(Request $request)
    {
        return \Validator::make($request->all(),[
            'titre' => 'required|string|max:255',
            'categorie_id' => 'required|integer',
            'description' => 'required|string|min:3',
        ])->validate();
    }

}


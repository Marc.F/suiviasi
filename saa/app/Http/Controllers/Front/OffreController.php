<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Offre;

class OffreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lesOffres = Offre::orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->paginate(9);
        return view ('front.offre.indexOffre')->with("lesOffres",$lesOffres);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('front.offre.createOffre');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $offre = new Offre();
        $this->save($offre, $request);
        
        return redirect()->route('front.offre.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Offre $offre)
    {
        return view ('front.offre.showOffre')->with('offre', $offre);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Offre $offre)
    {
        return view ('front.offre.editOffre')->with('offre', $offre);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offre $offre)
    {
        $this->validateData($request);

        $this->save($offre, $request);

        return redirect()->route('front.offre.show', $offre);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offre $offre)
    {
        $offre->delete();

        return redirect()->route('front.offre.index');
    }

    public function search(Request $request)
    {
        $search = $request->search;
        $lesOffres = Offre::where('titre', 'LIKE', "%".$search."%")->orWhere('ville', 'LIKE', "%".$search."%")->orderBy('created_at' , 'DESC', 'updated_at' ,'DESC')->latest()->paginate(9);
        return view ('front.offre.indexOffre', ['lesOffres' => $lesOffres]);
    }

    public function addFavorite(Offre $offre)
    {
        auth()->user()->favorisOffre()->attach($offre->id_offre);
        return redirect()->route('front.offre.show', $offre);
    }

    public function removeFavorite(Offre $offre)
    {
        auth()->user()->favorisOffre()->detach($offre->id_offre);
        return redirect()->route('front.offre.show', $offre);
    }

    private function save(Offre $offre, Request $request)
    {
        $offre->titre = $request->titre;
        $offre->description = $request->description;
        $offre->adresse = $request->adresse;
        $offre->ville = $request->ville;
        $offre->code_postal = $request->code_postal;
        $offre->mail = $request->mail;
        $offre->telephone = $request->telephone;
        $offre->entreprise = $request->entreprise;
        $offre->salaire = $request->salaire;
        $offre->niveau_etude = $request->niveau_etude;
        $offre->save();
    }

    private function validateData(Request $request)
    {
        return \Validator::make($request->all(),[
            'titre' => 'required|string|max:255',
            'description' => 'required|string|min:3',
            'adresse' => 'required|string|max:255',
            'ville' => 'required|string|max:255',
            'code_postal' => 'required|string|max:255',
            'mail' => 'required|string|max:255',
            'telephone' => 'required|string|max:255',
            'entreprise' => 'required|string|max:255',
            'salaire' => 'required|string|max:255',
            'niveau_etude' => 'required|string',
        ])->validate();
    }
}
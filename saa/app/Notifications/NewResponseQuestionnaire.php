<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewResponseQuestionnaire extends Notification
{
    use Queueable;
    private $questionnaire;
    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($questionnaire, $user)
    {
        $this->questionnaire = $questionnaire;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "message" => $this->user->completeName().' à répondu au questionnaire "'.$this->questionnaire->titre.'"',
            "url" => "/front/questionnaire/".$this->questionnaire->id_questionnaire
        ];
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\ReponseTopic;

class NewResponse extends Notification
{
    use Queueable;

    private $reponse;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reponse)
    {
        $this->reponse = $reponse;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable){

        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            "message" => $this->reponse->user->completeName()." a répondu au topic  ".$this->reponse->topic->titre,
            "url" => "/front/frontForum/".$this->reponse->topic->id_topic
        ];
    }
}

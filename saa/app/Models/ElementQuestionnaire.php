<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ElementQuestionnaire extends Model
{
    protected $primaryKey = 'id_element';

    protected $fillable = [
        'description', 'questionnaire_id'
    ];

    public function questionnaire()
    {
        return $this->belongsTo('App\Models\Questionnaire');
    }
}

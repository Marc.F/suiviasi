<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $primaryKey = 'id_questionnaire';

    protected $fillable = [
        'titre'
    ];

    public function elements()
    {
        return $this->hasMany('App\Models\ElementQuestionnaire', 'questionnaire_id');
    }
}

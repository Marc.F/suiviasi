<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'id_utilisateur';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom', 'prenom', 'email','telephone', 'password', 'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', 'api_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function completeName()
    {
        return $this->prenom." ".$this->nom;
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id');
    }

    public function offres()
    {
        return $this->hasMany('App\Models\Offre', 'utilisateur_id')->latest();
    }

    public function reponses()
    {
        return $this->hasMany('App\Models\ReponseElement');
    }

    public function topics()
    {
        return $this->hasMany('App\Models\Topic', 'utilisateur_id')->latest();
    }

    public function reponsesTopic()
    {
        return $this->belongsToMany('App\Models\Topic', 'reponse_topics', 'utilisateur_id', 'topic_id')->withPivot('description');
    }

    public function favorisTopic()
    {
        return $this->belongsToMany('App\Models\Topic', 'favoris_topics', 'utilisateur_id', 'topic_id')->withTimestamps();
    }

    public function favorisOffre()
    {
        return $this->belongsToMany('App\Models\Offre', 'favoris_offres', 'utilisateur_id', 'offre_id')->withTimestamps();
    }
}

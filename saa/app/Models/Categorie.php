<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $primaryKey = 'id_categorie';

    protected $fillable = [
        'titre_categorie', 'description_categorie', 'logo_categorie'
    ];

    public function topics()
    {
        return $this->hasMany('App\Models\Topic', 'categorie_id', 'id_categorie')->latest();
    }
}

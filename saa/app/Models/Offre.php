<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Offre extends Model
{
    protected $primaryKey = 'id_offre';

    protected $fillable = [
        'titre', 'description', 'adresse', 'ville', 'code_postale', 'mail', 'telephone', 'entreprise', 'salaire', 'niveau_etude', 'utilisateur_id'
    ];

    protected static function booted()
    {
        static::creating(function($offre){
            $offre->utilisateur_id = Auth::id();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'utilisateur_id');
    }

    public function favorisOffre()
    {
        return $this->belongsToMany('App\Models\User', 'favoris_offres', 'offre_id', 'utilisateur_id')->withTimestamps();
    }
}


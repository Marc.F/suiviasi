<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ReponseTopic extends Model
{
    protected $primaryKey = 'id_reponse';

    protected $fillable = [
        'description', 'topic_id', 'utilisateur_id'
    ];


    public function topic()
    {
        return $this->belongsTo('App\Models\Topic', 'topic_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'utilisateur_id');
    }
}

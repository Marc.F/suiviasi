<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReponseElement extends Model 
{
    protected $primaryKey = 'id_reponse';

    protected $fillable = [
        'description', 'element_id', 'reponse_id'
    ];

    public function element()
    {
        return $this->belongsTo('App\Models\ElementQuestionnaire');
    }

    public function utilisateur()
    {
        return $this->belongsTo('App\Models\User');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Topic extends Model
{
    protected $primaryKey = 'id_topic';

    protected $fillable = [

        'titre', 'description', 'cloturer', 'utilisateur_id', 'categorie_id'
    ];

    protected static function booted()
    {
        static::creating(function($topic){ 
            $topic->utilisateur_id = Auth::id();
        });
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'utilisateur_id');
    }

    public function reponsesTopic()
    {
        return $this->hasMany('App\Models\ReponseTopic', 'topic_id');
    }

    public function favorisTopic()
    {
        return $this->belongsToMany('App\Models\User', 'favoris_topics','topic_id', 'utilisateur_id')->withTimestamps();
    }

    public function categorie()
    {
        return $this->belongsTo('App\Models\Categorie', 'categorie_id');
    }
}


//définition des différents éléments
var notif = document.getElementById("notif");
var dropdown = document.getElementById("dropdown");
var notifBtn = document.getElementById("notifBtn");
var spanNotif = document.getElementById("spanNotif");
var message = document.getElementById("message");
var deleteNotifBtn = document.getElementById("deleteNotifBtn");
var emptyMessage = document.getElementById("emptyMessage");

//Notif toast
const Toast = swal.mixin({
    toast: true,
    position: "bottom-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
});

let oldResult = null;

//Fonction permettant de Vérifier le nombre de notification non lu et d'afficher celle ci sur l'interface en haut de l'îcone
const countUnreadNotif = () => {
    let url = "/front/notifications/unread";

    return fetch(url, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then((result) => {
            if (oldResult == null) {
                oldResult = result;
            }

            if (oldResult < result) {
                Toast.fire({
                    title:
                        '<i class="fas fa-bell fa-2x" style="color: #ffe135; margin-right: 1em; padding: 0.2em"></i> <p style="margin-top: 0.5em">Une nouvelle notification viens d\'arriver !</p>',
                });

                oldResult = result;
            }

            if (result > 0) {
                if (result >= 100) {
                    notif.innerHTML = "+99";
                } else {
                    notif.innerHTML = result;
                }

                notif.style.display = "flex";
                spanNotif.style.display = "inline-block";
            } else {
                notif.style.display = "none";
                spanNotif.style.display = "none";
            }
        })
        .catch((error) => console.log(error));
};

//Fonction permettant de récupérer toutes les notifications et de les afficher dans le dropdown
const getNotif = () => {
    let url = "/front/notifications/all";
    return fetch(url, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    })
        .then((response) => response.json())
        .then((result) => {
            while (message.hasChildNodes()) {
                message.removeChild(message.firstChild);
            }
            emptyMessage.style.display = "none";
            deleteNotifBtn.style.display = "block";
            dropdown.setAttribute(
                "class",
                "dropdown-content dropdown-content-not-empty"
            );

            if (result.length > 0) {
                result.forEach((notif) => {
                    if (notif.read_at == null) {
                        markAsRead();
                    }
                    let mainDiv = document.createElement("DIV");
                    mainDiv.setAttribute("class", "main-div-notif");

                    let a = document.createElement("A");
                    a.setAttribute("class", "div-notif");
                    a.setAttribute("href", notif.data.url);
                    a.setAttribute(
                        "onclick",
                        "delOneNotif('" + notif.id + "')"
                    );

                    let div = document.createElement("DIV");
                    div.setAttribute("class", "show-del-notif");
                    div.innerHTML =
                        '<i class="fas fa-times fa-2x icon-notif-del"></i>';
                    div.setAttribute(
                        "onclick",
                        "delOneNotif('" + notif.id + "')"
                    );

                    let p = document.createElement("P");
                    p.innerHTML = notif.data.message;

                    let p1 = document.createElement("P");
                    p1.setAttribute("class", "div-notif-date");
                    var date = new Date(notif.created_at);
                    p1.innerHTML = date.toLocaleString();

                    a.appendChild(p);
                    a.appendChild(p1);
                    mainDiv.appendChild(a);
                    mainDiv.appendChild(div);
                    message.appendChild(mainDiv);
                });
            } else {
                emptyMessage.style.display = "block";
                dropdown.setAttribute(
                    "class",
                    "dropdown-content dropdown-content-empty"
                );
                deleteNotifBtn.style.display = "none";
            }
        })
        .catch((error) => console.log(error));
};

//fonction permettant l'ajout la lecture des notifications lors du clik sur le notifBtn --> nregitrement en BDD dans read_at
const markAsRead = () => {
    let url = "/front/notifications/markasread";

    return fetch(url, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    }).catch((error) => console.log(error));
};

notifBtn.addEventListener("click", function () {
    if (dropdown.style.display == "none") {
        getNotif();
        dropdown.style.display = "block";
    } else {
        dropdown.style.display = "none";
    }
});

//Fonction permettant de supprimer toutes les notifications
deleteNotifBtn.addEventListener("click", function () {
    let url = "/front/notifications/delete";

    return fetch(url, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    })
        .then((response) => {
            getNotif();
        })
        .catch((error) => console.log(error));
});

//Fonction permettant de cacher le dropdown lors du click a côté de celui-ci
$(window).on("click", function (e) {
    var container = $("#dropdown");

    if (
        !container.is(e.target) &&
        container.has(e.target).length === 0 &&
        e.target.getAttribute("id") !== notifBtn.getAttribute("id")
    ) {
        container.hide();
    }
});

countUnreadNotif();

//appel de la fonction countUnreadNotif() toute les secondes
var interval = setInterval(function () {
    countUnreadNotif();
}, 1000);

//Fonction permettant de supprimer une seul notification grâce à son id
const delOneNotif = (id) => {
    let url = "/front/notifications/delete/" + id;
    const token = $('meta[name="csrf-token"]').attr("content");

    return fetch(url, {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": token,
        },
    })
        .then((response) => {
            getNotif();
        })
        .catch((error) => console.log(error));
};

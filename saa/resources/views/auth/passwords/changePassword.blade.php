@extends('layouts.auth')
@section('title_page', 'Changement du mot de passe')
@section('content')
@if (session('status'))
<div class="alert alert-success" role="alert">
    {{ session('status') }}
</div>
@endif
    <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt="" style="will-change: transform; transform: perspective(300px) rotateX(0deg) rotateY(0deg);">
					<img src="{{asset('front/images/logo_black.png') }}" alt="IMG">
				</div>

                <form class="login100-form validate-form" method="POST" action="{{ route('change-password') }}">
                    @csrf
                    @method('POST')
					<span class="login100-form-title">
						SAA - Changement du mot de passe
					</span>

                    <div class="wrap-input100 @error('password') alert-validate @enderror" data-validate="@error('password') {{$errors->first('password')}} @enderror">
						<input class="input100" type="password" name="password" id="password" placeholder="Mot de passe">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 @error('password_confirmation') alert-validate @enderror" data-validate="@error('password_confirmation') {{$errors->first('password_confirmation')}} @enderror">
						<input class="input100" type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirmer le mot de passe">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit">
							Changer
						</button>
					</div>

					<div class="text-center p-t-136">
                        Changement du mot de passe lors de la première connexion
					</div>
				</form>
			</div>
		</div>
    </div>
@endsection
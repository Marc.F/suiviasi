@extends('layouts.auth')
@section('title_page', 'Rénitialiser')
@section('content')
    <div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic js-tilt" data-tilt="" style="will-change: transform; transform: perspective(300px) rotateX(0deg) rotateY(0deg);">
					<img src="{{asset('front/images/logo_black.png') }}" alt="IMG">
				</div>

                <form class="login100-form validate-form" method="POST" action="{{ route('password.update') }}">
                    @csrf
                    @method('POST')
					<span class="login100-form-title">
						SAA - Réinitiliser votre mot de passe
					</span>

					<div class="wrap-input100 @error('email') alert-validate @enderror" data-validate="@error('email') {{$errors->first('email')}} @enderror">
                        <input class="input100" type="email" name="email" id="email" value="{{ $email ?? old('email') }}" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
                    </div>

                    <div class="wrap-input100 @error('password') alert-validate @enderror" data-validate="@error('password') {{$errors->first('password')}} @enderror">
                        <input class="input100" type="password" name="password" id="password" placeholder="Mot de passe">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
                    </div>

                   <div class="wrap-input100">
                        <input class="input100" type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirmer mot de passe">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="submit">
							Rénitialiser
						</button>
					</div>

					<div class="text-center p-t-136">
					</div>
				</form>
			</div>
		</div>
    </div>
@endsection
@extends('layouts.admin')
@section('title', 'Modification du role : '.$role->designation)

@section('content')
<div class="card card-primary">
    
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="{{route('role.update', $role)}}">
    @csrf
    @method('PATCH')
      <div class="card-body">
        <div class="row">
            <div class="col-md-6">

        <div class="form-group">
          <label for="exampleInputEmail1">Designation</label>
            <input type="text" class="form-control @error('designation') is-invalid @enderror" id="designation" name="designation" placeholder="Entrer un role " value="@if (old('designation')) {{old('designation')}} @else {{$role->designation}} @endif">    
          @error('designation') <span id="name-error" class="error invalid-feedback">{{$errors->first('designation')}}</span> @enderror
        </div>

   

    </div>
      </div>
      <!-- /.card-body -->

      

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Modifier</button>
      </div>
    </form>
  </div>

  
  @section('script')
  <script>
    $(function () {
          //Initialize Select2 Elements
        $('.select2bs4').select2({
        theme: 'bootstrap4'
        })
    })
  </script>
  @endsection
@endsection
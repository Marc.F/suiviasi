@extends('layouts.admin')
@section('title', 'Création de role')

@section('content')
<div class="card card-primary">
    
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="{{route('role.store')}}" >
    @csrf

    @method('POST')


      <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Designation Role</label>
                    <input type="text" class="form-control @error('designation') is-invalid @enderror" id="designation" name="designation" placeholder="Entrer un role " value="{{old('designation')}}">
                    @error('designation') <span id="name-error" class="error invalid-feedback">{{$errors->first('designation')}}</span> @enderror
                </div>
            </div>


    
      @section('script')
      <script>
        $(function () {
              //Initialize Select2 Elements
            $('.select2bs4').select2({
            theme: 'bootstrap4'
            })
        })
      </script>
      @endsection

    </div>

      
        
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Créer</button>
      </div>
    </form>
  </div>
@endsection
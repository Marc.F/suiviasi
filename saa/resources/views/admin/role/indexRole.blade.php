@extends('layouts.admin')
@section('title', 'Liste des Roles')


@section('content')

    <div class="col-md-2" style="margin-bottom:20px;">
        <a href="{{ route('role.create') }}"> <button type="button" class="btn btn-block btn-success">Créer un Role
                <i class="fas fa-plus-square"></i></button> </a>
    </div>
    <div class="card">

        <div class="card-body">
            <table id="table" class="table table-bordered table-striped table-hover dataTable" role="grid">
                <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Designation</th>
                        <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1"
                            style="text-align: right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($roles as $role)
                        <tr role="row" class="odd">
                            <td>{{$role->designation}}</td>
                            <td>
                                @if($role->designation != "admin" && $role->designation != "ancien_etudiant" && $role->designation != "etudiant" && $role->designation != "responsable")
                                    <div class="text-center">
                                        <a href="{{route('role.edit', $role)}}"><button type="button" class="btn btn-primary"><i class="fas fa-edit"></i></button></a>
                                        <button type="button" class="btn btn-danger remove-role" data-name="{{ $role->designation }}" data-action="{{ route('role.destroy', $role) }}"><i class="fas fa-trash"></i></button>
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>
@section('script')
    <script>
        $(function() {
            $("#table").DataTable({
                "responsive": true,
                "autoWidth": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                },
            });
        });

        $("body").on("click", ".remove-role", function() {

            var current_object = $(this);
            var name = current_object.attr('data-name');

            swal.fire({

                title: "Êtes vous sur ?",
                text: "Voulez-vous vraiment supprimer le role suivant : " + name + " ?",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#6c757d',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Supprimer !',
                reverseButtons: true

            }).then((result) => {

                if (result.value) {

                    var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('DELETE')');
                    $('body').find('.remove-form').submit();
                }

            });

        });

    </script>

@endsection

@endsection

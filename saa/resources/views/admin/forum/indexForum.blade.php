@extends('layouts.admin')
@section('title', 'Liste des Sujets')


@section('content')

<div class="col-md-2" style="margin-bottom:20px;">
  <a href="{{route('forum.create')}}">  <button type="button" class="btn btn-block btn-success">Créer un sujet <i class="fas fa-plus-square"></i></button> </a>
</div>


    <div class="card">

<div class="card-body">
    <table id="table" class="table table-bordered table-striped table-hover dataTable" role="grid">
    <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Titre</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Descritpion</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Colturer</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Créateur</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Catégorie</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="text-align: right">Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($forums as $forum)
        <tr role="row" class="odd">
            <td>{{$forum->titre}}</td>
            <td>{{substr($forum->description, 0, 50)."..."}} </td>
            <td>
            @if ($forum->cloturer)
                Oui
            @else
                Non
            @endif
            </td>
            <td>{{$forum->user->prenom}} {{$forum->user->nom}}</td>
            <td>{{$forum->categorie->titre_categorie}}</td>
            <td>
                <div class="text-center">
                    <a href="{{route('forum.edit', $forum)}}"><button type="button" class="btn btn-primary "><i class="fas fa-edit"></i></button></a>
                    <button type="button" class="btn btn-danger remove-forum" data-name="{{ $forum->titre }}" data-action="{{ route('forum.destroy', $forum) }}"><i class="fas fa-trash"></i></button>
                </div>
            </td>
        </tr>
    @endforeach
    </tbody>
  </table>
</div>
    </div>
</div>
@section('script')
    <script>
        $(function() {
            $("#table").DataTable({
                "responsive": true,
                "autoWidth": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                },
            });
        });

        $("body").on("click", ".remove-forum", function() {

            var current_object = $(this);
            var name = current_object.attr('data-name');

            swal.fire({

                title: "Êtes vous sur ?",
                text: "Voulez-vous vraiment supprimer le forum suivant : " + name + " ?",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#6c757d',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Supprimer !',
                reverseButtons: true

            }).then((result) => {

                if (result.value) {

                    var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('DELETE')');
                    $('body').find('.remove-form').submit();
                }

            });

        });

    </script>

@endsection    

@endsection
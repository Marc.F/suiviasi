@extends('layouts.admin')
@section('title', 'Création sujet')

@section('content')
<div class="card card-primary">
    
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="{{route('forum.store')}}" >
    @csrf

    @method('POST')


      <div class="card-body">
        <div class="row">
            <div class="col-md-6">

        <div class="form-group">
          <label for="exampleInputEmail1">Titre</label>
          <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre" name="titre" placeholder="Entrer un titre " value="{{old('titre')}}">
          @error('titre') <span id="name-error" class="error invalid-feedback">{{$errors->first('titre')}}</span> @enderror
        </div>
      </div>

      <div class="col-md-6" data-select2-id="69">
        <div class="form-group" data-select2-id="68">
            <label>Choix de la categorie</label>
            <select class="form-control select2bs4 select2-hidden-accessible @error('id_categorie') is-invalid @enderror" style="width: 100%;" data-select2-id="17" tabindex="-1" aria-hidden="true" name="categorie_id" id="categorie_id">
                @foreach($categories as $categorie)
                <option value="{{$categorie->id_categorie}}">{{$categorie->titre_categorie}}</option>
                @endforeach
            </select>
            @error('id_categorie') <span id="name-error" class="error invalid-feedback">{{$errors->first('id_categorie')}}</span> @enderror
          </div>
      </div>

      <div class="col-md-12">
        <div class="form-group">
            <label>Description du sujet</label>
            <textarea class="form-control @error('description') is-invalid @enderror" rows="3" placeholder="Entrer une description" name="description" id="description"></textarea> 
            @error('description') <span id="name-error" class="error invalid-feedback">{{$errors->first('description')}}</span> @enderror
          </div>
      </div>

      @section('script')
      <script>
        $(function () {
              //Initialize Select2 Elements
            $('.select2bs4').select2({
            theme: 'bootstrap4'
            })
        })
      </script>
      @endsection

    </div>

      
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Créer</button>
      </div>
    </form>
  </div>
@endsection
@extends('layouts.admin')

@section('titleBanner', 'SAA')

@section('textBanner', 'Bienvenue')


@section('content')
    
<div class="text-center">
    <h1>Panneau d'administration</h1><br><br>
  </div>



<div class="card-deck">
    <div class="zoom">
    <a class="card bg-primary" href="{{route('offre.index')}}" class="nav-link {{ request()->routeIs('offre.*') ? 'active' : ''}}">
      <div class="card-body text-center"><i class="nav-icon fas fa-comments"></i>
        <p class="card-text">Offres</p>
      </div>
    </a>
    </div>

    <div class="zoom">
    <a class="card bg-primary" href="{{route('forum.index')}}" class="nav-link {{ request()->routeIs('forum.*') ? 'active' : ''}}">
      <div class="card-body text-center"><i class="nav-icon fas fa-book-reader"></i>
        <p class="card-text">Sujet</p>
      </div>
    </a>
    </div>

    <div class="zoom">
        <a class="card bg-primary" href="{{route('questionnaire.index')}}" class="nav-link {{ request()->routeIs('questionnaire.*') ? 'active' : ''}}">
          <div class="card-body text-center"><i class="nav-icon fas fa-question-circle"></i>
            <p class="card-text">Questionnaire</p>
          </div>
        </a>
    </div>


    <div class="zoom">
        <a class="card bg-primary" href="{{route('user.index')}}" class="nav-link {{ request()->routeIs('user.*') ? 'active' : ''}}">
             <div class="card-body text-center"><i class="nav-icon fas fa-users"></i>
              <p class="card-text">Utilisateurs</p>
            </div>
          </a>
    </div>

    <div class="zoom">
    <a class="card bg-primary" href="{{route('role.index')}}" class="nav-link {{ request()->routeIs('role.*') ? 'active' : ''}}">
      <div class="card-body text-center"><i class="nav-icon fas fa-key"></i>
        <p class="card-text">Roles</p>
      </div>
    </a>
    </div>
    
    <div class="zoom">
    <a class="card bg-primary" href="{{route('categorie.index')}}" class="nav-link {{ request()->routeIs('categorie.*') ? 'active' : ''}}">
      <div class="card-body text-center"><i class="nav-icon fas fa-grip-horizontal"></i>
        <p class="card-text">Catégories</p>
      </div>
    </a>
    </div>
  </div>

</div>



<!-- Debut Statistiques -->

<!-- Offres -->
<div class="row">
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3>{{$nbOffre}}</h3>

        <p>Offres</p>
      </div>
      <div class="icon">
        <i class="nav-icon fas fa-comments"></i>
      </div>
    </div>
  </div>


  <!-- Forum -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3>{{$nbTopicNonCloturer}}</h3>

        <p>Sujet non cloturer</p>
      </div>
      <div class="icon">
        <i class="nav-icon fas fa-book-reader"></i>
      </div>
    </div>
  </div>


  <!-- Questionnaire -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3>{{($nbQuestionnaire)}}</h3>
        
        <p>Questionnaire</p>
      </div>
      <div class="icon">
        <i class="nav-icon fas fa-question-circle"></i>
      </div>
    </div>
  </div>


  <!-- Utilisateurs -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
      <div class="inner">
        <h3>{{$nbUser}}</h3>

        <p>Utilisateurs</p>
      </div>
      <div class="icon">
        <i class="nav-icon fas fa-users"></i>
      </div>
    </div>
  </div>



  <!-- ./col -->
</div>

<!-- Fin Statistiques -->

@endsection
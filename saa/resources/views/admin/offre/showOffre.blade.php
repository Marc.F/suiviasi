@extends('layouts.admin')
@section('title', 'Affichage de l offre : '.$offre->titre)

@section('content')
<div class="card card-primary">
    
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="{{route('offre.show', $offre)}}">
    @csrf
    @method('PATCH')
      <div class="card-body">
        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label for="exampleInputEmail1">Titre</label>
                    <input disabled type="text" class="form-control" id="titre" name="titre" placeholder="Entrer un titre" value="@if (old('titre')) {{old('titre')}} @else {{$offre->titre}} @endif">
                </div>
            </div>


      <div class="col-md-12">
        <div class="form-group">
            <label>Description de l'offre</label>
            <textarea disabled class="form-control" rows="3" name="description" id="description" placeholder="Entrer une description" value="{{old('description')}}" name="description" id="description">@if (old('description')) {{old('description')}} @else {{$offre->description}} @endif</textarea>
          </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Adresse</label>
            <input disabled type="text" class="form-control" id="adresse" name="adresse" placeholder="Entrer une adresse" value="@if (old('adresse')) {{old('adresse')}} @else {{$offre->adresse}} @endif">
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Ville</label>
            <input disabled type="text" class="form-control" id="ville" name="ville" placeholder="Entrer une ville" value="@if (old('ville')) {{old('ville')}} @else {{$offre->ville}} @endif">
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Code Postal</label>
            <input disabled type="text" class="form-control" id="code_postal" name="code_postal" placeholder="Entrer un code postal" value="@if (old('code_postal')) {{old('code_postal')}} @else {{$offre->code_postal}} @endif">
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Mail</label>
            <input disabled type="text" class="form-control" id="mail" name="mail" placeholder="Entrer un mail" value="@if (old('mail')) {{old('mail')}} @else {{$offre->mail}} @endif">
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Téléphone</label>
            <input disabled type="text" class="form-control" id="telephone" name="telephone" placeholder="Entrer un N° de téléphone" value="@if (old('telephone')) {{old('telephone')}} @else {{$offre->telephone}} @endif">
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Entreprise</label>
            <input disabled type="text" class="form-control" id="entreprise" name="entreprise" placeholder="Entrer une entreprise" value="@if (old('entreprise')) {{old('entreprise')}} @else {{$offre->entreprise}} @endif">
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Salaire</label>
            <input disabled type="text" class="form-control" name="salaire" placeholder="Entrer un salaire" value="@if (old('salaire')) {{old('salaire')}} @else {{$offre->salaire}} @endif">
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Niveau Etude</label>
            <input disabled type="text" class="form-control" name="niveau_etude" placeholder="Entrer un niveau d'étude" value="@if (old('niveau_etude')) {{old('niveau_etude')}} @else {{$offre->niveau_etude}} @endif">
        </div>
      </div>


        
      </div>
      <!-- /.card-body -->

      

      <div class="card-footer">
      <a href="{{route('offre.index')}}" class="btn btn-primary">Retour</a>
      </div>
    </form>
  </div>

  
  @section('script')
  <script>
    $(function () {
          //Initialize Select2 Elements
        $('.select2bs4').select2({
        theme: 'bootstrap4'
        })
    })
  </script>
  @endsection
@endsection
@extends('layouts.admin')
@section('title', 'Création offre')

@section('content')
<div class="card card-primary">
    
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="{{route('offre.store')}}" >
    @csrf

    @method('POST')


      <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="exampleInputEmail1">Titre</label>
                    <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre" name="titre" value="{{old('titre')}}" placeholder="Entrer un titre ">
                    @error('titre') <span id="name-error" class="error invalid-feedback">{{$errors->first('titre')}}</span> @enderror
                </div>
            </div>


      <div class="col-md-12">
        <div class="form-group">
            <label>Description de l'offre</label>
            <textarea class="form-control @error('description') is-invalid @enderror" rows="3" placeholder="Entrer une description" name="description" id="description">{{old('description')}}</textarea> 
            @error('description') <span id="name-error" class="error invalid-feedback">{{$errors->first('description')}}</span> @enderror
          </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Adresse</label>
            <input type="text" class="form-control @error('adresse') is-invalid @enderror" id="adresse" name="adresse" placeholder="Entrer une adresse" value="{{old('adresse')}}">
            @error('adresse') <span id="name-error" class="error invalid-feedback">{{$errors->first('adresse')}}</span> @enderror
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Ville</label>
            <input type="text" class="form-control @error('ville') is-invalid @enderror" id="ville" name="ville" placeholder="Entrer une ville" value="{{old('ville')}}">
            @error('ville') <span id="name-error" class="error invalid-feedback">{{$errors->first('ville')}}</span> @enderror
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Code Postal</label>
            <input type="text" class="form-control @error('code_postal') is-invalid @enderror" id="code_postal" name="code_postal" placeholder="Entrer un code postal" value="{{old('code_postal')}}">
            @error('code_postal') <span id="name-error" class="error invalid-feedback">{{$errors->first('code_postal')}}</span> @enderror
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Mail</label>
            <input type="text" class="form-control @error('mail') is-invalid @enderror" id="mail" name="mail" placeholder="Entrer un mail" value="{{old('mail')}}">
            @error('mail') <span id="name-error" class="error invalid-feedback">{{$errors->first('mail')}}</span> @enderror
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Téléphone</label>
            <input type="text" class="form-control @error('telephone') is-invalid @enderror" id="telephone" name="telephone" placeholder="Entrer un N° de téléphone" value="{{old('telephone')}}">
            @error('telephone') <span id="name-error" class="error invalid-feedback">{{$errors->first('telephone')}}</span> @enderror
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Entreprise</label>
            <input type="text" class="form-control @error('entreprise') is-invalid @enderror" id="entreprise" name="entreprise" placeholder="Entrer une entreprise" value="{{old('entreprise')}}">
            @error('entreprise') <span id="name-error" class="error invalid-feedback">{{$errors->first('entreprise')}}</span> @enderror
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Salaire</label>
            <input type="text" class="form-control @error('salaire') is-invalid @enderror" id="salaire" name="salaire" placeholder="Entrer un salaire" value="{{old('salaire')}}">
            @error('salaire') <span id="name-error" class="error invalid-feedback">{{$errors->first('salaire')}}</span> @enderror
        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label for="exampleInputEmail1">Niveau Etude</label>
            <input type="text" class="form-control @error('niveau_etude') is-invalid @enderror" id="niveau_etude" name="niveau_etude" placeholder="Entrer un niveau d'étude" value="{{old('niveau_etude')}}">
            @error('niveau_etude') <span id="name-error" class="error invalid-feedback">{{$errors->first('niveau_etude')}}</span> @enderror
        </div>
      </div>

      @section('script')
      <script>
        $(function () {
              //Initialize Select2 Elements
            $('.select2bs4').select2({
            theme: 'bootstrap4'
            })
        })
      </script>
      @endsection

    </div>

      
        
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Créer</button>
      </div>
    </form>
  </div>
@endsection
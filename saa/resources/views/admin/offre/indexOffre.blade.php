@extends('layouts.admin')
@section('title', 'Liste des offres')


@section('content')
<div class="col-md-2" style="margin-bottom:20px;">
  <a href="{{route('offre.create')}}">  <button type="button" class="btn btn-block btn-success">Créer une offre <i class="fas fa-plus-square"></i></button> </a>
</div>




    <div class="card">
<div class="card-body">
    <table id="table" class="table table-bordered table-striped table-hover dataTable" role="grid">
    <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Titre</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Adresse</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Ville</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Code Postal</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Mail</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Téléhpone</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Entreprise</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Salaire</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Niveau Etude</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="text-align: right">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($offres as $offre)
            <tr role="row" class="odd">
                <td>{{$offre->titre}}</td>
                <td>{{$offre->adresse}}</td>
                <td>{{$offre->ville}}</td>
                <td>{{$offre->code_postal}}</td>
                <td>{{$offre->mail}}</td>
                <td>{{$offre->telephone}}</td>
                <td>{{$offre->entreprise}}</td>
                <td>{{$offre->salaire}}</td>
                <td>{{$offre->niveau_etude}}</td>
                <td>
                    <div class="text-center">
                        <a href="{{route('offre.edit', $offre)}}"><button type="button" class="btn btn-primary "><i class="fas fa-edit"></i></button></a>
                        <button type="button" class="btn btn-danger remove-offre" data-name="{{ $offre->titre }}" data-action="{{ route('offre.destroy', $offre) }}"><i class="fas fa-trash"></i></button>
                        <a href="{{route('offre.show', $offre)}}"><button type="button" class="btn btn-info"><i class="fas fa-eye"></i></button></a>
                      </div>
                </td>
            </tr>
        @endforeach
    </tbody>
  </table>
</div>
    </div>
</div>

@section('script')
    <script>
        $(function() {
            $("#table").DataTable({
                "responsive": true,
                "autoWidth": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                },
            });
        });

        $("body").on("click", ".remove-offre", function() {

            var current_object = $(this);
            var name = current_object.attr('data-name');

            swal.fire({

                title: "Êtes vous sur ?",
                text: "Voulez-vous vraiment supprimer l'offre suivante : " + name + " ?",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#6c757d',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Supprimer !',
                reverseButtons: true

            }).then((result) => {

                if (result.value) {

                    var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('DELETE')');
                    $('body').find('.remove-form').submit();
                }

            });

        });

    </script>

@endsection        
@endsection






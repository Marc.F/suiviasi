@extends('layouts.admin')
@section('title', 'Liste des Catégories')


@section('content')

<div class="col-md-2" style="margin-bottom:20px;">
  <a href="{{route('categorie.create')}}">  <button type="button" class="btn btn-block btn-success">Créer une catégorie <i class="fas fa-plus-square"></i></button> </a>
</div>

    <div class="card">
    <div class="card-body">
    <table id="table" class="table table-bordered table-striped table-hover dataTable" role="grid">
    <thead>
        <tr role="row">
            <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Titre</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Description</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Logo</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Créer le</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Modifié le</th>
            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="text-align: right">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($categories as $categorie)
            <tr role="row" class="odd">
                <td>{{$categorie->titre_categorie}}</td>
                <td>{{$categorie->description_categorie}}</td>
                <td><img src="{{asset('/storage/'.$categorie->logo_categorie)}}" width="45"></td>
                <td>{{$categorie->created_at}}</td>
                <td>{{$categorie->updated_at}}</td>
                <td>
                    <div class="text-center">
                        <a href="{{route('categorie.edit', $categorie)}}"><button type="button" class="btn btn-primary "><i class="fas fa-edit"></i></button></a>
                        <button type="button" class="btn btn-danger remove-categorie" data-name="{{ $categorie->titre_categorie}}" data-action="{{ route('categorie.destroy', $categorie) }}"><i class="fas fa-trash"></i></button>
                      </div>
                </td>
            </tr>
        @endforeach
        </tbody>
          </table>
        </div>
    </div>
    @section('script')
    <script>
        $(function() {
            $("#table").DataTable({
                "responsive": true,
                "autoWidth": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                },
            });
        });

        $("body").on("click", ".remove-categorie", function() {

            var current_object = $(this);
            var name = current_object.attr('data-name');

            swal.fire({

                title: "Êtes vous sur ?",
                text: "Voulez-vous vraiment supprimer la catégorie suivante : " + name + " ?",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#6c757d',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Supprimer !',
                reverseButtons: true

            }).then((result) => {

                if (result.value) {

                    var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('DELETE')');
                    $('body').find('.remove-form').submit();
                }

            });

        });

    </script>

@endsection 

@endsection

@extends('layouts.admin')
@section('title', 'Création Catégorie')

@section('content')
    <div class="card card-primary">

        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{ route('categorie.store') }}" enctype="multipart/form-data" >
            @csrf
            @method('POST')
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Titre</label>
                            <input type="text" class="form-control @error('titre_categorie') is-invalid @enderror"
                                id="titre_categorie" name="titre_categorie" placeholder="Entrer un titre " value="{{ old('titre_categorie') }}">
                            @error('titre_categorie') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('titre_categorie') }}</span> @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="logo_categorie">Logo de la catégorie</label>
                            <div class="input-group">
                                <div class="custom-file" style="display: unset;">
                                    <input type="file" class="custom-file-input  @error('logo_categorie') is-invalid @enderror" id="logo_categorie" name="logo_categorie">
                                    @error('logo_categorie') <span id="name-error"
                                    class="error invalid-feedback">{{ $errors->first('logo_categorie') }}</span> @enderror
                                    <label class="custom-file-label" for="logo_categorie">Choisir un fichier png,jpg,jpeg....</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="form-control @error('description_categorie') is-invalid @enderror" rows="3"
                                placeholder="Entrer une description" name="description_categorie"
                                id="description_categorie" value="{{ old('description_categorie') }}"></textarea>
                            @error('description_categorie') <span id="name-error"
                                    class="error invalid-feedback">{{ $errors->first('description_categorie') }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Créer</button>
            </div>
        </form>

    @section('script')
        <script type="text/javascript">
            $(document).ready(function() {
                bsCustomFileInput.init();
            });

        </script>
    @endsection
</div>
@endsection

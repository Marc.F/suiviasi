@extends('layouts.admin')
@section('title', 'Modification du forum : '.$categorie->titre_categorie)

@section('content')
<div class="card card-primary">
    
    <!-- /.card-header -->
    <!-- form start -->
    <form method="POST" action="{{route('categorie.update', $categorie)}}" enctype="multipart/form-data">
      @csrf
      @method('PATCH')
        <div class="card-body">
          <div class="row">
              <div class="col-md-6">
  
          <div class="form-group">
            <label for="exampleInputEmail1">Titre</label>
              <input type="text" class="form-control @error('titre_categorie') is-invalid @enderror" id="titre_categorie" name="titre_categorie" placeholder="Entrer un titre " value="@if (old('titre_categorie')) {{old('titre_categorie')}} @else {{$categorie->titre_categorie}} @endif">    
            @error('titre_categorie') <span id="name-error" class="error invalid-feedback">{{$errors->first('titre_categorie')}}</span> @enderror
          </div>
        </div>
  
        <div class="col-md-6">
            <div class="form-group">
                <label for="logo_categorie">Logo de la catégorie</label>
                <div class="input-group">
                    <div class="custom-file" style="display: unset;">
               <input type="file" class="custom-file-input  @error('logo_categorie') is-invalid @enderror" id="logo_categorie" name="logo_categorie"  value="@if (old('logo_categorie')) {{old('logo_categorie')}} @else {{$categorie->logo_categorie}} @endif"> 
                         @error('logo_categorie') <span id="name-error" class="error invalid-feedback">{{ $errors->first('logo_categorie') }}</span> @enderror
                        <label class="custom-file-label" for="logo_categorie">@if($categorie->logo_categorie){{$categorie->logo_categorie}}@else Choisir un fichier png,jpg,jpeg....@endif</label>
                    </div>
                </div>
            </div>
        </div>

        
  
        <div class="col-md-12">
            <div class="form-group">
                <label>Description du forum</label>
                <textarea class="form-control @error('description_categorie') is-invalid @enderror" rows="3" placeholder="Entrer une description" value="{{old('description_categorie')}}" name="description_categorie" id="description_categorie">@if (old('description_categorie')) {{old('description_categorie')}} @else {{$categorie->description_categorie}} @endif</textarea>
                @error('description_categorie') <span id="name-error" class="error invalid-feedback">{{$errors->first('description_categorie')}}</span> @enderror
              </div>
            </div>
        </div>
  
        
          
        </div>
        <!-- /.card-body -->
  
        
  
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Modifier</button>
        </div>
      </form>
    </div>
  
    @section('script')
        <script type="text/javascript">
            $(document).ready(function() {
                bsCustomFileInput.init();
            });

        </script>
    @endsection





    
@endsection
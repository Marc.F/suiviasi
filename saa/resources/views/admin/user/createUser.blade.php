@extends('layouts.admin')
@section('title', 'Création un utilisateur')

@section('content')
    <div class="card card-primary">
        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{ route('user.store') }}" enctype="multipart/form-data">
            @csrf

            @method('POST')

            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nom</label>
                            <input type="text" class="form-control @error('nom') is-invalid @enderror" id="nom" name="nom"
                                placeholder="Entrer un nom..." value="{{ old('nom') }}">
                            @error('nom') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('nom') }}</span> @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Prenom</label>
                            <input type="text" class="form-control @error('prenom') is-invalid @enderror" id="prenom"
                                name="prenom" placeholder="Entrer un prenom..." value="{{ old('prenom') }}">
                            @error('prenom') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('prenom') }}</span> @enderror
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control @error('email') is-invalid @enderror" id="email"
                                name="email" placeholder="Entrer un email..." value="{{ old('email') }}">
                            @error('email') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('email') }}</span> @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Telephone</label>
                            <input type="text" class="form-control @error('telephone') is-invalid @enderror" id="telephone"
                                name="telephone" placeholder="Entrer un telephone..." value="{{ old('telephone') }}">
                            @error('telephone') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('telephone') }}</span> @enderror
                        </div>
                    </div>

                    <div class="col-md-6" data-select2-id="69">
                        <div class="form-group" data-select2-id="68">
                            <label>Choix du role</label>
                            <select
                                class="form-control select2bs4 select2-hidden-accessible @error('role_id') is-invalid @enderror"
                                style="width: 100%;" data-select2-id="17" tabindex="-1" aria-hidden="true"
                                name="role_id" id="role_id">
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id_role }}">{{ $role->designation }}</option>
                                @endforeach
                            </select>
                            @error('role_id') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('role_id') }}</span> @enderror
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Mot de passe</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                id="password" name="password" placeholder="Entrer un mot de passe...">
                            @error('password') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('password') }}</span> @enderror
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Confirmer le mot de passe</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                id="password_confirmation" name="password_confirmation"
                                placeholder="Confirmer le mot de passe...">
                        </div>
                    </div>
                </div>
                @section('script')
                    <script>
                        $(function() {
                            //Initialize Select2 Elements
                            $('.select2bs4').select2({
                                theme: 'bootstrap4'
                            })
                        });

                    </script>
                @endsection

            </div>

        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Créer</button>
        </div>
    </form>
</div>
@endsection

@extends('layouts.admin')
@section('title', 'Liste des Utilisateurs')
@section('content')

    <div class="col-md-2" style="margin-bottom:20px;">
        <a href="{{ route('user.create') }}"> <button type="button" class="btn btn-block btn-success">Créer un Utilisateur
                <i class="fas fa-plus-square"></i></button> </a>
    </div>
    <div class="card">

        <div class="card-body">
            <table id="table" class="table table-bordered table-striped table-hover dataTable" role="grid">
                <thead>
                    <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Nom</th>
                        <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Prenom</th>
                        <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Email</th>
                        <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Telephone</th>
                        <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Role</th>
                        <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Actif</th>
                        <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Editer le </th>
                        <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Créer le </th>
                        <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1"
                            style="text-align: right">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                        <tr role="row" class="odd">
                            <td>{{ $user->nom }}</td>
                            <td>{{ $user->prenom }} </td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->telephone }}</td>
                            <td>{{ $user->role->designation }}</td>
                            <td>@if($user->active) oui @else non @endif</td>
                            <td>{{ $user->updated_at }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>
                                <div class="text-center">
                                    <a href="{{ route('user.edit', $user) }}"><button type="button"
                                            class="btn btn-primary "><i class="fas fa-edit"></i></button></a>
                                    <button class="btn btn-danger remove-user" data-name="{{ $user->completeName()}}" data-action="{{ route('user.destroy', $user) }}"><i class="fas fa-trash"></i></button>
                                    @if($user->active)
                                    <button class="btn btn-success active-user" data-name="{{ $user->completeName()}}" data-active="{{$user->active}}" data-action="{{ route('user.active', $user) }}"><i class="fas fa-user-check"></i></button>
                                    @elseif(!$user->active && $user->nom != "supprimé")
                                    <button class="btn btn-danger active-user" data-name="{{ $user->completeName()}}" data-active="{{$user->active}}" data-action="{{ route('user.active', $user) }}"><i class="fas fa-user-times"></i></button>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@section('script')
    <script>
        $(function() {
            $("#table").DataTable({
                "responsive": true,
                "autoWidth": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                },
            });
        });

        $("body").on("click", ".active-user", function() {

            var current_object = $(this);
            var name = current_object. attr('data-name');
            var active = current_object. attr('data-active');
            var text;
            var textButton;

            if(active == 1)
            {
                text = "Voulez-vous vraiment désactiver l'utilisateur suivant : " + name + " ?"
                textButton = "Désactiver"
            }
            else
            {
                text = "Voulez-vous vraiment activer l'utilisateur suivant : " + name + " ?"
                textButton = "Activer"
            }

            swal.fire({

                title: "Êtes vous sur ?",
                text: text,
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#6c757d',
                confirmButtonColor: '#dc3545',
                confirmButtonText: textButton,
                reverseButtons: true

            }).then((result) => {

                if (result.value) {

                    var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('PATCH')');
                    $('body').find('.remove-form').submit();
                }

            });

        });

        $("body").on("click", ".remove-user", function() {

        var current_object = $(this);
        var name = current_object. attr('data-name');

        swal.fire({

            title: "Êtes vous sur ?",
            text: "Voulez-vous vraiment supprimer l'utilisateur suivant : " + name + " ?",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#6c757d',
            confirmButtonColor: '#dc3545',
            confirmButtonText: 'Supprimer !',
            reverseButtons: true

        }).then((result) => {

            if (result.value) {

                var action = current_object.attr('data-action');

                $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                    "'></form>");
                $('body').find('.remove-form').append('@csrf');
                $('body').find('.remove-form').append('@method('DELETE')');
                $('body').find('.remove-form').submit();
            }

        });

        });

    </script>

@endsection

@endsection

@extends('layouts.admin')
@section('title', 'Affichage du questionnaire : '.$questionnaire->titre)

@section('content')
    <div class="col-md-2" style="margin-bottom:20px;">
        <a href="{{route("elements-questionnaire.create", $questionnaire)}}">  <button type="button" class="btn btn-block btn-success"> Ajout d'un élément <i class="fas fa-plus-square">
                </i></button> </a>
    </div>
    <div class="card card-primary">

        <!-- /.card-header -->
        <!-- form start -->
        @csrf
        <div class="card">
            <div class="card-body">
                <table id="table" class="table table-bordered table-striped table-hover dataTable" role="grid">
                    <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="table" rowspan="1" colspan="1">Question</th>
                            <th class="sorting" tabindex="0" aria-controls="table" rowspan="1" colspan="1"
                                style="text-align: right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($elements as $element)
                        <tr role="row" class="odd">
                            <td>{{$element->description}}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{route('elements-questionnaire.edit', [$questionnaire, $element])}}"><button type="button" class="btn btn-primary "><i class="fas fa-edit"></i></button></a>
                                    <button type="button" class="btn btn-danger remove-element" data-name="{{$element->description}}" data-action="{{ route('elements-questionnaire.destroy', [$questionnaire, $element]) }}"><i class="fas fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.card-body -->



        <div class="card-footer">
            <a href="{{route('questionnaire.index')}}" class="btn btn-primary">Retour</a>
        </div>
    </div>


@section('script')
    <script>
        $(function() {
            $("#table").DataTable({
                "responsive": true,
                "autoWidth": false,
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                },
            });
        });

        $("body").on("click", ".remove-element", function() {

            var current_object = $(this);
            var name = current_object.attr('data-name');

            swal.fire({

                title: "Êtes vous sur ?",
                text: "Voulez-vous vraiment supprimer le questionnaire suivant : " + name + " ?",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#6c757d',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Supprimer !',
                reverseButtons: true

            }).then((result) => {

                if (result.value) {

                    var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('DELETE')');
                    $('body').find('.remove-form').submit();
                }

            });

        });

    </script>

@endsection
@endsection

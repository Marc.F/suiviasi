@extends('layouts.admin')
@section('title', 'Creation de l \'élément du questionnaire : '. $questionnaire->titre)

@section('content')
    <div class="card card-primary">
        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" enctype="multipart/form-data" action="{{route('elements-questionnaire.store', $questionnaire)}}" >
            @csrf
            @method('POST')
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <input type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Entrer une description ">
                            @error('description') <span id="name-error" class="error invalid-feedback">{{$errors->first('description')}}</span> @enderror
                        </div>
                    </div>
                    @section('script')
                        <script>
                            $(function () {
                                //Initialize Select2 Elements
                                $('.select2bs4').select2({
                                    theme: 'bootstrap4'
                                })
                            })
                        </script>
                    @endsection

                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Créer</button>
            </div>
        </form>
    </div>
@endsection

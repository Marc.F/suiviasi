@extends('layouts.admin')
@section('title', 'Modification de l\'élément : '.$element->description.' appartenant au questionnaire '.$questionnaire->titre)

@section('content')
    <div class="card card-primary">

        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{route('elements-questionnaire.update', [$questionnaire, $element])}}">
            @csrf
            @method('PATCH')
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Titre</label>
                            <input type="text" class="form-control @error('description') is-invalid @enderror" id="description" name="description" placeholder="Entrez une description ..." value="@if (old('description')) {{old('description')}} @else {{$element->description}} @endif">
                            @error('description') <span id="name-error" class="error invalid-feedback">{{$errors->first('description')}}</span> @enderror
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Modifier</button>
                </div>
        </form>
    </div>

@section('script')
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    </script>
@endsection
@endsection

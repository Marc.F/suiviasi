@extends('layouts.admin')
@section('title', 'Modification de l offre : '.$questionnaire->titre)

@section('content')
    <div class="card card-primary">

        <!-- /.card-header -->
        <!-- form start -->
        <form method="POST" action="{{route('questionnaire.update', $questionnaire)}}">
            @csrf
            @method('PATCH')
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">

                        <div class="form-group">
                            <label for="exampleInputEmail1">Titre</label>
                            <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre" name="titre" placeholder="Entrer un titre " value="@if (old('titre')) {{old('titre')}} @else {{$questionnaire->titre}} @endif">
                            @error('titre') <span id="name-error" class="error invalid-feedback">{{$errors->first('titre')}}</span> @enderror
                        </div>
                    </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Modifier</button>
                </div>
        </form>
    </div>

@section('script')
    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    </script>
@endsection
@endsection

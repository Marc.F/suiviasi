@extends('layouts.front')
@section('titleBanner')
Liste des questionnaires
@endsection

@section('content')
			<section class="wrapper">
				<div class="inner">
					<div class="header-topic">
					<div id='divBoutonRepondre'>
						<a href="{{route('home')}}"><button class="btn btn-primary" style=""><i class="fas fa-home" style="margin-right: 1em"></i>Accueil</button></a>
					</div>
							<form method="POST" action="{{route('front.questionnaire.search')}}" class="form-search-topic">
								@csrf
								@method('POST')
			
								<input type="text" name ="search" id="search" style="width: 20em;height: 2em;border-radius: 4px 0px 0px 4px;border-right: none;background-color: white;" placeholder="Rechercher ...">
								<button type="submit" class="btn icon-search-topic"><i class="fas fa-search"></i></button>
							</form>
					</div>
					<div class="highlights">
						@foreach ($lesQuestionnaires as $questionnaire)
						<section>
							<a href="{{route('front.questionnaire.show', $questionnaire)}}">

							<div class="rotate" style="height: 100%;">
									<div class="content">
											<header>
												<?php $i = 0 ?>
												@foreach ($lesRepondu as $repondu)
													@if($repondu->id_questionnaire == $questionnaire->id_questionnaire)
														<span class="fas fa-question-circle" style="font-size: 4em;margin-bottom: 0.5em;color: red;"></span>
														<?php $i++ ?>
														@break;
													@endif
												@endforeach
												@if($i == 0)
													<span class="fas fa-check-circle" style="font-size: 4em;margin-bottom: 0.5em;color: green;"></span>
												@endif
												<span class='dernierMessage_lien'>
													<h3 class='cardTitle overflowHide'>{{$questionnaire->titre}}</h3>
												</span>
                                            </header>
                                            <?php $i =0;?>
                                            @foreach ($questionnaire->elements as $element)
                                                <span class='dernierMessage_lien'><p class='cardIntituler overflowHide'><span class='cardName' style="color: grey">{{$element->description}}</span></p></span>
                                                <?php $i++;?>
                                            @endforeach
                                            @if ($questionnaire->elements->count() > 3)
                                                <span class='dernierMessage_lien'><p class='cardIntituler overflowHide'><span class='cardName'>...</span></p></span>    
                                            @endif
									</div>
							</div>
							</a>						
						</section>
						@endforeach						
					</div>
					@if(count($lesQuestionnaires->links()->elements[0]) > 1)
					<div class="pagination pagination-front">		
						<div class="pagination">
							{{$lesQuestionnaires->links()}}
						</div>
					</div>
					@endif
				</div>
			</section>
@endsection
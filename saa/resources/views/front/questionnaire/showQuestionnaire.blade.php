@extends('layouts.front')
@section('titleBanner')
Questionnaire
@endsection

@section('content')
    <section class="wrapper">
        <div class="inner">
            <div>
			    <a href="{{ url()->previous() }}"><button class="btn btn-primary">Retour</button></a>
		    </div>
            <div class="highlights contentReponse">
                <div class="contentForumCategorieTopicTopic_titre" >
                    <div class="forumCategorieTopicTopic_titre" style="margin: 0.5em 1em 0 1em !important;font-size: 2em;">
                            {{$questionnaire->titre}}
                    </div>
                </div>
            </div>
            <form method="POST" action="{{route('front.questionnaire.reponseStore', $questionnaire)}}" >
                @csrf
                @method('POST')
                
                <?php $count = 1 ?>
                @foreach ($questionnaire->elements as $element)
                <div class="contentForumCategorieTopicTopic_description" style="width: max-content !important;font-size : 1em; margin-bottom: 0.5em; font-style: italic">
                    <div class='forumTopCategorieTopic' style="display: block; width:auto;">
                        <?php echo($count.".")?> {{$element->description}}
                        <?php $count++ ?>
                    </div>
                </div>
                <div style="margin-bottom: 2em">
                    <textarea style="resize: none; background-color: rgb(255, 255, 255); height: 3.3em;overflow-y: hidden;" name='{{$element->description}}' id='{{$element->description}}'></textarea>
                </div>
                @endforeach
                <div id="divEnvoieReponseQuestionnaire">
                    <input type="submit" class="btn btn-primary" id="envoieReponseQuestionnaire" style="margin-top: 1em" value="Envoyer"> 
                </div> 
            </form>
        </div>
    </section>
    @section('script')
    <script type="text/javascript">
        //Redimension des textareas
        window.addEventListener('resize', function(){
            $( ".autoSize" ).each(function() {	
                $(this).height('1px');
                $(this).height((this.scrollHeight - 20)+"px");
            });
        });
        $( ".autoSize" ).each(function() {
            $(this).height('1px');
            $(this).height((this.scrollHeight - 20)+"px");
        });

        $("textarea").keyup(function(e) {
            while($(this).outerHeight() < this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                $(this).height($(this).height() + 1);
            };
            while($(this).outerHeight() > this.scrollHeight + parseFloat($(this).css("borderTopWidth")) + parseFloat($(this).css("borderBottomWidth"))) {
                $(this).height($(this).height() - 1);
            };
        });

        //Chargement des données (si il y en as)
        const elements = {!! json_encode($questionnaire->elements) !!};
        elements.forEach(element => {
            reponseTextarea(element);
        });
        
        //Affichage des données + bloquer le textarea
        function reponseTextarea(element){
            const reponseElements = {!! json_encode($reponseElements) !!};
                        
            reponseElements.forEach(reponseElement => {
                if(reponseElement.element_id == element.id_element)
                {
                    document.getElementById(element.description).innerText= reponseElement.description;
                    document.getElementById(element.description).disabled = true;
                    document.getElementById(element.description).style.backgroundColor =  "#E1E1E1";
                    document.getElementById("divEnvoieReponseQuestionnaire").hidden = true;
                }
            });
        }

    </script>
    @endsection
@endsection
@extends('layouts.front')
@section('titleBanner')
    Nouveau questionnaire
@endsection

@section('content')

    <section class="wrapper">
        <div class="inner">
            <div id='divBoutonRepondre'>
                <a href="{{ route('front.questionnaire.index') }}"><button class="btn btn-primary">Retour</button></a>
            </div>
            <form id="form" method="POST" action="{{ route('front.questionnaire.store') }}">
                @csrf

                @method('POST')

                <div class="highlights" style="margin: 2.5em 0.5rem 0 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin">
                            <label for="titre">Titre</label>
                            <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre"
                                name="titre" placeholder="Entrer un titre " value="{{old('titre')}}">
                            @error('titre') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('titre') }}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="highlights" style="margin: 2.5em 0.5rem 0 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin" id="formQuestion1">
                            <label for="titre">Question n°1</label>
                            <input type="text" id="question1" name="question1" placeholder="Entrer une question">
                        </div>
                    </div>
                </div>

                <div id="newQuestion"></div>

                <div>
                    <button type="button" style="margin: 1em 0;" onclick="ajoutQuestion()"class="btn btn-primary"><i class="fas fa-plus" style="color:green;"></i>  Ajouter une question</button>
                </div>

                <!-- Bouton -->
                <div id='divBoutonRepondre'>
                    <button type="submit" class="btn btn-primary">Créer</button>
                </div>
            </form>
        </div>
    </section>

@section('script')
    <script>
        var count = 2
        
        function ajoutQuestion(){
            var preCount = count-1
            var preText = "question"+ preCount
            var preId = "formQuestion" + preCount
            if(document.getElementById(preText).value == ""){
                var labelErreur = document.createElement('label')
                labelErreur.setAttribute("style", "color:red;")
                labelErreur.setAttribute("id", "label")
                labelErreur.innerHTML = "Remplir le champ précédent avant d'ajouter une nouvelle question !"
                document.getElementById(preId).appendChild(labelErreur)
            }else{

                if(document.getElementById("label") != null){
                    document.getElementById(preId).removeChild(document.getElementById("label"))
                }
                var divHighlights = document.createElement('div')
                divHighlights.className = "highlights"
                divHighlights.style.margin = 0

                var divTitre = document.createElement('div')
                divTitre.className = "contentForumCategorieTopicTopic_titre"

                var divForm = document.createElement('div')
                divForm.className = "form-group margin"
                var id = "formQuestion" + count
                divForm.setAttribute("id", id)

                var label = document.createElement('label')
                label.innerHTML = "Question n°" + count;

                var inputQuestion = document.createElement('input')
                var text = "question" + count
                inputQuestion.setAttribute("id", text)
                inputQuestion.setAttribute("name", text)
                inputQuestion.setAttribute("type", "text")
                inputQuestion.setAttribute("placeHolder", "Entrer une question")

                divForm.appendChild(label)
                divForm.appendChild(inputQuestion)
                divTitre.appendChild(divForm)
                divHighlights.appendChild(divTitre)

                document.getElementById('newQuestion').appendChild(divHighlights)

                count++
            }
        }

        $(function() {
            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })
    </script>
@endsection

@endsection

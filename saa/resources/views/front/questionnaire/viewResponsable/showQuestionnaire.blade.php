@extends('layouts.front')
@section('titleBanner')
Questionnaire
@endsection

@section('content')
    <section class="wrapper">
        <div class="inner">
            <div>
			    <a href="{{route('front.questionnaire.index')}}"><button class="btn btn-primary">Retour</button></a>
                    <a href="{{route('front.questionnaire.edit', $questionnaire)}}"><button class="btn btn-primary" style=""><i class="fas fa-pencil-alt" style="margin-right: 0.5em"></i>Modifier questionnaire</button></a>
		    </div>
            <div class="highlights contentReponse">
                <div class="contentForumCategorieTopicTopic_titre" >
                    <div class="forumCategorieTopicTopic_titre" style="margin: 0.5em 1em 0 1em !important;font-size: 2em;">
                            {{$questionnaire->titre}}
                    </div>
                </div>
            </div>
            <div style="display : flex;">
                <div style="width: 50%; padding: 0.5em; border-style: solid; border-width:0.15em; border-color : white;"> 
                    <div class="contentForumCategorieTopicTopic_description" style="width: max-content !important;font-size : 1.2em; margin-bottom: 0.5em; font-weight: bold;">
                        <div class='forumTopCategorieTopic' style="display: block; width:auto;">
                            Questions
                        </div>
                    </div>
                    <?php $count = 1 ?>
                    <div class="contentForumCategorieTopicTopic_description" style="width: max-content !important;font-size : 1em; margin-bottom: 0.5em; font-style: italic">
                        <div class='forumTopCategorieTopic' style="display: block; width:auto;">
                        @foreach ($questionnaire->elements as $element)
                            <?php echo($count.".")?> {{$element->description}} <br>
                            <?php $count++ ?>
                            @endforeach
                        </div>
                    </div>
                    
                </div>
                <div style="width: 50%; padding: 0.5em; border-style: solid; border-width:0.15em; border-color : white;"> 
                    <div style="display:flex; flex-wrap: wrap;">
                        <div class="contentForumCategorieTopicTopic_description" style="width: max-content !important;font-size : 1.2em; margin-bottom: 0.5em; font-weight: bold;">
                            <div class='forumTopCategorieTopic' style="display: block; width:auto;">
                                Nombres de questionnaire rempli : {{count($utilisateurRepondu)}}
                            </div>
                        </div>
                    @if(count($utilisateurRepondu) != 0)
                        <div style="margin-bottom: 0.5em; margin-left:0.5em; padding-top: 0.3em"> 
                            <a href="{{route('front.questionnaire.allpdf', $questionnaire)}}" class="btn btn-danger">Tous convertir en PDF</a>
                        </div>
                    </div>
                    
                        <div class="contentForumCategorieTopicTopic_description" style="width: max-content !important;font-size : 1.2em; margin-bottom: 0.5em; font-weight: bold;">
                            <div class='forumTopCategorieTopic' style="display: block; width:auto;">
                                Réponses : 
                            </div>
                        </div>
                    @endif
                    @foreach ($utilisateurRepondu as $utilisateur)
                        <div style="margin-top:0.5em;">
                            <a href="{{route('front.questionnaire.showUser', [$questionnaire, $utilisateur])}}" style="margin-left:2em;"><button class="btn btn-primary" style="font-style: italic">
                                        {{$utilisateur->nom}} {{$utilisateur->prenom}}
                            </button></a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    @section('script')
    <script type="text/javascript">
        //Redimension des textareas
        window.addEventListener('resize', function(){
            $( ".autoSize" ).each(function() {	
                $(this).height('1px');
                $(this).height((this.scrollHeight - 20)+"px");
            });
        });
        $( ".autoSize" ).each(function() {
            $(this).height('1px');
            $(this).height((this.scrollHeight - 20)+"px");
        });
    </script>
    @endsection
@endsection
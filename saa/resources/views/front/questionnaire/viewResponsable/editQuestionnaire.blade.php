@extends('layouts.front')
@section('titleBanner')
    Mon questionnaire
@endsection

@section('content')
<section class="wrapper">
        <div class="inner">
            <div id='divBoutonRepondre'>
                <a href="{{ route('front.questionnaire.index') }}"><button class="btn btn-primary">Retour</button></a>
            </div>
            <form id="form" method="POST" action="{{ route('front.questionnaire.update', $questionnaire) }}">
                @csrf

                @method('PATCH')

                <div class="highlights" style="margin: 2.5em 0.5rem 2.5rem 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin">
                            <label for="titre">Titre</label>
                            <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre"
                                name="titre" placeholder="Entrer un titre " value="@if (old('titre')) {{old('titre')}} @else {{$questionnaire->titre}} @endif">
                            @error('titre') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('titre') }}</span> @enderror
                        </div>
                    </div>
                </div>

                <?php $count = 1 ?>
                
                @foreach ($questionnaire->elements as $element)
                <div class="highlights" style="margin: 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin">
                            <label for="titre">Question n°<?php echo $count ?></label>
                            <input type="text" name="question_<?php echo $count;$count++?>" placeholder="Entrer une question" value="{{$element->description}}">
                        </div>
                    </div>
                </div>
                @endforeach
                

                <!-- Bouton -->
                <div id='divBoutonRepondre'>
                    <button type="submit" class="btn btn-primary">Modifier</button>
                </div>
            </form>
        </div>
    </section>
@endsection
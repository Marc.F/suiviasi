@extends('layouts.front')
@section('titleBanner', 'Offre d\'emploi')

@section('link')
<link rel="stylesheet" href={{asset('adminlte/css/adminlte.css') }} />
@endsection

@section('content')
<section class="wrapper">
  <div class="inner">
  
      <div id='divBoutonRepondre'>
        <a href="{{route('front.offre.index')}}"><button class="btn boutonOffrePrimary" style="">Retour</button></a>
      </div>
      <div class="highlights">
        <div class="wrap-login100" style="width: 100%; padding: 0">
          <div class="content_card">
              <div class="card-header">
                <h4 style="font-size : 2.1rem;"class="card-title">{{$offre->titre}}</h4>
                <div class='celluleDroiteTopicTopic'>
                  @if(auth()->user()->favorisOffre->find($offre->id_offre) == null)
                    <a><button type="button" onclick="location.href='{{route('front.offre.addFavorite', $offre)}}'" class="btn btnTopic btnEdit" style="margin-right: 0.5em;"><i class="fas fa-heart" style="margin-right: 0.5em; color: red;"></i>Ajouter aux favoris</button></a>
                  @else
                    <a><button type="button" onclick="location.href='{{route('front.offre.removeFavorite', $offre)}}'" class="btn btnTopic btnDelete" style="margin-right: 0.5em;"><i class="fas fa-heart-broken" style="margin-right: 0.5em; color: darkred"></i>Supprimer des favoris</button></a>
                  @endif
                  @if($offre->user->id_utilisateur == auth()->user()->id_utilisateur)
                    <a href="{{route('front.offre.edit', $offre)}}"><button id="btnModifier"type="button" class="btn btnTopic btnEdit" style="margin-right: 0.5em"><i class="fas fa-pencil-alt" style="margin-right: 0.5em"></i>Modifier</button></a>
                  @endif
                      <form method="POST" action="{{route('front.offre.update', $offre)}}" style="margin: 0" id="formEditReponse{{$offre->id_offre}}" style="display: none">
                              @csrf
                              @method('PATCH')
                              <textarea class='textAreaTopic' id='modifReponse{{$offre->id_offre}}'  style="display: none" name='modifReponse'></textarea>
                      </form>
                  @if(auth()->user()->Role->designation == 'admin' OR $offre->user->id_utilisateur == auth()->user()->id_utilisateur)
                      <a><button  id="btnSupprimer" data-action='{{route('front.offre.destroy', $offre)}}' class="btn btnTopic btnDelete remove-offre"><i class="fas fa-trash" style="margin-right: 0.5em;"></i>Supprimer</button></a>
                  @endif
                  @if(auth()->user()->favorisOffre->find($offre->id_offre) != null)
                    <i class="fa fa-thumb-tack" aria-hidden="true" style="font-size: 25px;margin-left: 15px;"></i>
                  @endif
                  </div> 
              </div>
              <div class="card-body">
                <div class="form-group">
                  <label>Entreprise</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="width: 3rem"><i class="fas fa-building"></i></span>
                    </div>
                    <textarea style=" width: calc(100% - 3rem) ;background: #e9ecef; border: 1px solid #ced4da;border-radius: 3px; resize:none;" readonly class="unfocus autoSize modifier-Offre">{{$offre->entreprise}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label>Description du poste</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="width: 3rem"><i class="fas fa-greater-than"></i></span>
                    </div>
                    <textarea style=" width: calc(100% - 3rem) ;background: #e9ecef; border: 1px solid #ced4da;border-radius: 3px; resize:none;" readonly class="unfocus autoSize modifier-Offre">{{$offre->description}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label>Niveau Requis</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="width: 3rem"><i class="fas fa-graduation-cap"></i></span>
                    </div>
                    <textarea style=" width: calc(100% - 3rem) ;background: #e9ecef; border: 1px solid #ced4da;border-radius: 3px; resize:none;" readonly class="unfocus autoSize modifier-Offre">{{$offre->niveau_etude}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label>Salaire</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="width: 3rem"><i class="fas fa-euro-sign"></i></span>
                    </div>
                    <textarea style=" width: calc(100% - 3rem) ;background: #e9ecef; border: 1px solid #ced4da;border-radius: 3px; resize:none;" readonly class="unfocus autoSize modifier-Offre">{{$offre->salaire}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="width: 3rem"><i class="fas fa-at"></i></span>
                    </div>
                    <textarea style=" width: calc(100% - 3rem) ;background: #e9ecef; border: 1px solid #ced4da;border-radius: 3px; resize:none;" readonly class="unfocus autoSize modifier-Offre">{{$offre->mail}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label>Numéro de téléphone</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="width: 3rem"><i class="fas fa-phone-alt"></i></span>
                    </div>
                    <textarea style=" width: calc(100% - 3rem) ;background: #e9ecef; border: 1px solid #ced4da;border-radius: 3px; resize:none;" readonly class="unfocus autoSize modifier-Offre">{{$offre->telephone}}</textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label>Localisation</label>
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text" style="width: 3rem"><i class="fas fa-location-arrow"></i></span>
                    </div>
                    <textarea style=" width: calc(100% - 3rem) ;background: #e9ecef; border: 1px solid #ced4da;border-radius: 3px; resize:none;" readonly class="unfocus autoSize modifier-Offre">{{$offre->adresse}} {{$offre->code_postal}} {{$offre->ville}}</textarea>
                    <iframe style="margin-top: 20px;" width="100%" height="450" frameborder="0" style="border:0"src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBFw0Qbyq9zTFTd-tUY6dZWTgaQzuU17R8&q={{str_replace(" ","+",$offre->adresse)."+".$offre->code_postal."+".str_replace(" ","+",$offre->ville)}}" allowfullscreen ></iframe>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
  </div>
</section>
@section('script')
    <script type="text/javascript">
    window.addEventListener('resize', function(){
            $( ".autoSize" ).each(function() {	
                $(this).height('1px');
                $(this).height((this.scrollHeight - 20)+"px");
            });
        });
        $( ".autoSize" ).each(function() {
            $(this).height('1px');
            $(this).height((this.scrollHeight - 20)+"px");
    });
        $("body").on("click", ".remove-offre", function() {

            var current_object = $(this);
            swal.fire({

                title: "Êtes vous sur ?",
                text: "Etes vous sur de supprimer l'offre ? (action irréversible)",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#6c757d',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Supprimer',
                reverseButtons: true

            }).then((result) => {

                if (result.value) {

                    var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('DELETE')');
                    $('body').find('.remove-form').submit();
                }

            });

        });
    </script>
@endsection
@stop

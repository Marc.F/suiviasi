@extends('layouts.front')
@section('titleBanner', 'Modification d\'une offre')

@section('link')
<link rel="stylesheet" href={{asset('adminlte/css/adminlte.css') }} />
@endsection

@section('content')
<section class="wrapper">
  <div class="inner">
  
      <div id='divBoutonRepondre'>
        <a href="{{route('front.offre.show', $offre)}}"><button class="btn boutonOffrePrimary" style="">Retour</button></a>
      </div>
      <div class="highlights">
        <div class="wrap-login100" style="width: 100%; padding: 0">
          <div class="content_card">
            <div class="card-header">
              <form method="POST" action="{{route('front.offre.update', $offre)}}" >
                @csrf
            
                @method('PATCH')
            
            
                  <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Titre</label>
                                <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre" name="titre" value="@if (old('titre')) {{old('titre')}} @else {{$offre->titre}} @endif">
                                @error('titre') <span id="name-error" class="error invalid-feedback">{{$errors->first('titre')}}</span> @enderror
                            </div>
                        </div>
            
            
                  <div class="col-md-12">
                    <div class="form-group">
                        <label>Description de l'offre</label>
                        <textarea style="min-height: 3.25rem; resize: none; overflow:hidden" class="form-control @error('description') is-invalid @enderror autoSize" rows="3" placeholder="Entrer une description" value="{{old('description')}}" name="description" id="description">@if (old('description')) {{old('description')}} @else {{$offre->description}} @endif</textarea> 
                        @error('description') <span id="name-error" class="error invalid-feedback">{{$errors->first('description')}}</span> @enderror
                      </div>
                  </div>
            
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Adresse</label>
                        <input type="text" class="form-control @error('adresse') is-invalid @enderror" id="adresse" name="adresse" placeholder="Entrer une adresse" value="@if (old('adresse')) {{old('adresse')}} @else {{$offre->adresse}} @endif">
                        @error('adresse') <span id="name-error" class="error invalid-feedback">{{$errors->first('adresse')}}</span> @enderror
                    </div>
                  </div>
            
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Ville</label>
                        <input type="text" class="form-control @error('ville') is-invalid @enderror" id="ville" name="ville" placeholder="Entrer une ville" value="@if (old('ville')) {{old('ville')}} @else {{$offre->ville}} @endif">
                        @error('ville') <span id="name-error" class="error invalid-feedback">{{$errors->first('ville')}}</span> @enderror
                    </div>
                  </div>
            
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Code Postal</label>
                        <input type="text" class="form-control @error('code_postal') is-invalid @enderror" id="code_postal" name="code_postal" placeholder="Entrer un code postal" value="@if (old('code_postal')) {{old('code_postal')}} @else {{$offre->code_postal}} @endif">
                        @error('code_postal') <span id="name-error" class="error invalid-feedback">{{$errors->first('code_postal')}}</span> @enderror
                    </div>
                  </div>
            
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Mail</label>
                        <input type="text" class="form-control @error('mail') is-invalid @enderror" id="mail" name="mail" placeholder="Entrer un mail" value="@if (old('mail')) {{old('mail')}} @else {{$offre->mail}} @endif">
                        @error('mail') <span id="name-error" class="error invalid-feedback">{{$errors->first('mail')}}</span> @enderror
                    </div>
                  </div>
            
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Téléphone</label>
                        <input type="text" class="form-control @error('telephone') is-invalid @enderror" id="telephone" name="telephone" placeholder="Entrer un N° de téléphone" value="@if (old('telephone')) {{old('telephone')}} @else {{$offre->telephone}} @endif">
                        @error('telephone') <span id="name-error" class="error invalid-feedback">{{$errors->first('telephone')}}</span> @enderror
                    </div>
                  </div>
            
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Entreprise</label>
                        <input type="text" class="form-control @error('entreprise') is-invalid @enderror" id="entreprise" name="entreprise" placeholder="Entrer une entreprise" value="@if (old('entreprise')) {{old('entreprise')}} @else {{$offre->entreprise}} @endif">
                        @error('entreprise') <span id="name-error" class="error invalid-feedback">{{$errors->first('entreprise')}}</span> @enderror
                    </div>
                  </div>
            
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Salaire</label>
                        <input type="text" class="form-control @error('salaire') is-invalid @enderror" id="salaire" name="salaire" placeholder="Entrer un salaire" value="@if (old('salaire')) {{old('salaire')}} @else {{$offre->salaire}} @endif">
                        @error('salaire') <span id="name-error" class="error invalid-feedback">{{$errors->first('salaire')}}</span> @enderror
                    </div>
                  </div>
            
                  <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Niveau Etude</label>
                        <input type="text" class="form-control @error('niveau_etude') is-invalid @enderror" id="niveau_etude" name="niveau_etude" placeholder="Entrer un niveau d'étude" value="@if (old('niveau_etude')) {{old('niveau_etude')}} @else {{$offre->niveau_etude}} @endif">
                        @error('niveau_etude') <span id="name-error" class="error invalid-feedback">{{$errors->first('niveau_etude')}}</span> @enderror
                    </div>
                  </div>
                </div>    
              </div>
            </div>
          </div>
        </div>
      </div>
        <button type="submit" class="btn btn-primary">Modifier</button>
    </form>
  </div>
</section>
@section('script')
    <script type="text/javascript">
        window.addEventListener('resize', function(){
                $( ".autoSize" ).each(function() {	
                    $(this).height('1px');
                    $(this).height((this.scrollHeight)+"px");
                });
            });
        window.addEventListener('keypress', function(){
                $( ".autoSize" ).each(function() {	
                    $(this).height('1px');
                    $(this).height((this.scrollHeight)+"px");
                });
        });
        $( ".autoSize" ).each(function() {
            $(this).height('1px');
            $(this).height((this.scrollHeight)+"px");
        });     
    </script>
@endsection
@stop

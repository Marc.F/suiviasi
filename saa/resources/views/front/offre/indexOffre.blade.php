@extends('layouts.front')
@section('titleBanner', 'Liste d\'offres d\'emploi')

@section('content')
			<section class="wrapper">
				<div class="inner">
					<div class="header-topic">
						<div id='divBoutonRepondre'>
							<a href="{{route('home')}}"><button class="btn btn-primary"><i class="fas fa-home" style="margin-right: 1em"></i>Accueil</button></a>
							@if(auth()->user()->Role->designation != 'etudiant')
								<a href="{{route('front.offre.create')}}"><button class="btn btn-primary"><i class="fas fa-plus" style="margin-right: 1em"></i>Ajouter une offre</button></a>
							@endif
						</div>
							<form method="POST" action="{{route('front.offre.search')}}" class="form-search-topic" >
								@csrf
								@method('POST')
			
								<input type="text" name ="search" id="search" style="width: 20em;height: 2em;border-radius: 4px 0px 0px 4px;border-right: none;background-color: white;" placeholder="Rechercher ...">
								<button type="submit" class="btn icon-search-topic"><i class="fas fa-search"></i></button>
							</form>
					</div>
					<div class="highlights">
						@foreach ($lesOffres as $offre)
						<section>
							<a href="{{route('front.offre.show', $offre)}}">
							<div class="rotate">
									@if(auth()->user()->favorisOffre->find($offre->id_offre) != null)
										<div class="celluleDroiteTopicTopic" style="margin: 1.2em 1.2em 0em 0em;width: auto;">
											<i class="fa fa-thumb-tack" aria-hidden="true" style="font-size: 25px;margin-left: 15px;"></i>
										</div>
									@endif
									<div class="content">
											<header>
												<span class="icon fa-vcard-o"></span>
												<span class='dernierMessage_lien'>
													<h3 class='cardTitle overflowHide'>{{$offre->titre}}</h3>
												</span>
											</header>
											<span class='dernierMessage_lien'><p class='cardIntituler overflowHide'>Entreprise : <span class='cardName'>{{$offre->entreprise}}</span></p></span>
											<span class='dernierMessage_lien'><p class='cardIntituler overflowHide'>Ville : <span class='cardName'>{{$offre->ville}}</span></p></span>
											<span class='dernierMessage_lien'><p class='cardIntituler overflowHide'>Salaire : <span class='cardName'>{{$offre->salaire}}</span></p></span>
											<span class='dernierMessage_lien'><p class='cardIntituler overflowHide'>Niveau Etude : <span class='cardName'>{{$offre->niveau_etude}}</span></p></span>
									</div>
							</div>
							</a>						
						</section>
						@endforeach
					</div>
					@if(count($lesOffres->links()->elements[0]) > 1)
					<div class="pagination pagination-front">		
						<div class="pagination">
							{{$lesOffres->links()}}
						</div>
					</div>
					@endif
				</div>
			</section>
@endsection
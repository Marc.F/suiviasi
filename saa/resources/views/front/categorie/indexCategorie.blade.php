@extends('layouts.front')
@section('titleBanner')
    Liste des catégories
@endsection

@section('content')
    <section class="wrapper">
        <div class="inner">
            <div id='divBoutonRepondre' style="display: block ruby">
                <a href="{{ route('home') }}"><button class="btn btn-primary" style=""><i class="fas fa-home" style="margin-right: 1em"></i>Accueil</button></a>
                <div style="float: right">
                    <a href="{{ route('front.forum.create') }}">
                        <button class="btn btn-primary"><i class="fas fa-plus" style="margin-right: 1em"></i>Nouveau sujet</button>
                    </a>
                    @if (count(auth()->user()->Topics) > 0)
                        <a href="{{ route('front.mesForum.index') }}">
                            <button class="btn btn-primary"><i class="fas fa-comment-dots" style="margin-right: 1em"></i>Mes sujets</button>
                        </a>
                    @endif
                </div>
            </div>
            <div class="highlights">
                @foreach ($lesCategories as $categorie)
                    <section>
                        <a href="{{ route('front.categorie.show', $categorie) }}">
                            <div class="rotate">
                                <div class="contentForumCategorie">
                                    <div class='forumTopCategorie'>
                                        <span class='iconeCategorie'>
                                            <img class='iconeCategorie'
                                                src="{{ asset('/storage' . $categorie->logo_categorie) }}" width="45">
                                        </span>
                                        <div class="forumCategorie_illustration">
                                            <div class="forumCategorie_titre">{{ $categorie->titre_categorie }}</div>
                                            <div class="forumCategorie_description">{{ $categorie->description_categorie }}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='forumBotCategorie'>
                                        <hr>
                                        <strong>
                                            <span class='dernierMessage'>Dernier topic :</span>
                                        </strong>
                                        <div class='dernierMessage_lien'>
                                            @if (count($categorie->topics) != 0)
                                                {{ optional($categorie->topics->last())->titre }}
                                            @else
                                                Aucun sujet
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </section>
                @endforeach
            </div>
            @if (count($lesCategories->links()->elements[0]) > 1)
                <div class="pagination pagination-front">
                    <div class="pagination">
                        {{ $lesCategories->links() }}
                    </div>
                </div>
            @endif
        </div>
    </section>
@endsection

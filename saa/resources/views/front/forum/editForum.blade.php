@extends('layouts.front')
@section('titleBanner')
Mon sujet
@endsection

@section('content')
    <section class="wrapper">
        <div class="inner">
            <form method="POST" action="{{route('front.forum.update', $topic)}}" >
                @csrf
                @method('PATCH')
                <div class="highlights contentReponse" style="margin: 2.5em 0.5rem 0 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin">
                            <label for="titre">Titre</label>
                            <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre" name="titre" placeholder="Entrer un titre " value="@if (old('titre')) {{old('titre')}} @else {{$topic->titre}} @endif">    
                            @error('titre') <span id="name-error" class="error invalid-feedback" value="{{old('titre')}}">{{$errors->first('titre')}}</span> @enderror
                        </div>
                    </div>
                </div>
                
                <div class="highlights contentReponse" style="margin: 1em 0.5rem 0 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin" data-select2-id="68">
                            <label>Choix de la categorie</label>
                            <select style="width: 100%;" data-select2-id="17" tabindex="-1" aria-hidden="true"
                                name="categorie_id" id="categorie_id">
                                @foreach ($categories as $categorie)
                                    <option @if($categorie == $topic->categorie) selected @endif value="{{$categorie->id_categorie}}">{{$categorie->titre_categorie}}</option>
                                        {{ $categorie->titre_categorie }}
                                    </option>
                                @endforeach
                            </select>
                                
                            @error('id_categorie') <span id="name-error" class="error invalid-feedback">{{$errors->first('id_categorie')}}</span> @enderror
                        </div>
                    </div>
                </div>
                <div class="highlights contentReponse" style="margin: 1em 0.5rem 0 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin">
                            <label>Description du sujet</label>
                            <textarea class="form-control @error('description') is-invalid @enderror" rows="3" placeholder="Entrer une description" value="{{old('description')}}" name="description" id="description">@if (old('description')) {{old('description')}} @else {{$topic->description}} @endif</textarea>
                            @error('description') <span id="name-error" class="error invalid-feedback">{{$errors->first('description')}}</span> @enderror
                        </div>
                    </div>
                </div>
                <!-- Bouton -->
                <div id='divBoutonRepondre'>
                    <button type="submit" class="btn btn-primary" >Modifier</button>
                    <a href="{{route('front.forum.show', [$topic->id_topic])}}"><button class="btn btnSupprimer">Annuler</button></a>
                </div>
            </form>  
        </div>
    </section>

    @section('script')
    <script>
    $(function () {
            //Initialize Select2 Elements
        $('.select2bs4').select2({
        theme: 'bootstrap4'
        })
    })
    </script>
    @endsection

@endsection


@extends('layouts.front')
@section('titleBanner')
    Nouveau sujet
@endsection

@section('content')

    <section class="wrapper">
        <div class="inner">
            <div id='divBoutonRepondre'>
                <a href="{{ route('front.categorie.index') }}"><button class="btn btn-primary">Retour</button></a>
            </div>
            <form method="POST" action="{{ route('front.forum.store') }}">
                @csrf

                @method('POST')

                <div class="highlights" style="margin: 2.5em 0.5rem 0 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin">
                            <label for="titre">Titre</label>
                            <input type="text" class="form-control @error('titre') is-invalid @enderror" id="titre"
                                name="titre" placeholder="Entrer un titre "
                                value="{{old('titre')}}">
                            @error('titre') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('titre') }}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="highlights" style="margin: 1em 0.5rem 0 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin" data-select2-id="68">
                            <label>Choix de la categorie</label>
                            <select style="width: 100%;" data-select2-id="17" tabindex="-1" aria-hidden="true"
                                name="categorie_id" id="categorie_id">
                                @foreach ($lesCategories as $categorie)
                                    <option value="{{ $categorie->id_categorie }}">
                                        {{ $categorie->titre_categorie }}
                                    </option>
                                @endforeach
                            </select>
                            @error('id_categorie') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('id_categorie') }}</span> @enderror
                        </div>
                    </div>
                </div>

                <div class="highlights contentReponse" style="margin: 1em 0.5rem 0 0;">
                    <div class="contentForumCategorieTopicTopic_titre">
                        <div class="form-group margin">
                            <label>Description du sujet</label>
                            <textarea class="form-control @error('description') is-invalid @enderror" rows="3"
                                placeholder="Entrer une description" name="description" id="description"></textarea>
                            @error('description') <span id="name-error"
                                class="error invalid-feedback">{{ $errors->first('description') }}</span> @enderror
                        </div>
                    </div>
                </div>
                <!-- Bouton -->
                <div id='divBoutonRepondre'>
                    <button type="submit" class="btn btn-primary">Créer</button>
                </div>
            </form>
        </div>
    </section>

@section('script')
    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })

    </script>
@endsection

@endsection

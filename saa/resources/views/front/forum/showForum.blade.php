@extends('layouts.front')
@section('titleBanner')
Sujet
@endsection

@section('content')
    <section class="wrapper">
        <div class="inner">
            <div>
			    <a href="{{route('front.categorie.show', $topic->categorie)}}"><button class="btn btn-primary">Retour</button></a>
                <a href="#reponse" ><button class="btn btn-primary" onClick="addTextArea()">Répondre</button></a>
		    </div>
            <div class="highlights contentReponse">
                <div class="contentForumCategorieTopicTopic_titre" >
                    <div class='forumTopCategorieTopic' style="justify-content: flex-end;">
                        <div class="celluleDroiteTopicTopic" style="display: block;">
                            @if(auth()->user()->favorisTopic->find($topic->id_topic) == null)
                                <button type="button" onclick="location.href='{{route('front.forum.addFavorite', $topic)}}'" class="btn btnTopic btnEdit" style="margin-right: 0.5em;"><i class="fas fa-heart" style="margin-right: 0.5em; color: red;"></i> Ajouter aux favoris</button>
                            @else
                                <button type="button" onclick="location.href='{{route('front.forum.removeFavorite', $topic)}}'" class="btn btnTopic btnDelete" style="margin-right: 0.5em;"><i class="fas fa-heart-broken" style="margin-right: 0.5em; color: darkred"></i>Supprimer des favoris</button>
                            @endif
                            @if($topic->user->id_utilisateur == auth()->user()->id_utilisateur)
                                <a href="{{route('front.forum.edit', $topic)}}"><button class="btn btnTopic btnEdit" style="margin-right: 0.5em;"><i class="fas fa-pencil-alt" style="margin-right: 0.5em"></i> Modifier</button></a>
                            @endif
                            @if($topic->user->id_utilisateur == auth()->user()->id_utilisateur OR auth()->user()->Role->designation == 'admin')
                                <button  id="btnSupprimer{{$topic->id_topic}}" data-action='{{route('front.forum.destroy', $topic)}}' class="btn btnTopic btnDelete remove-topic" style="margin-right: 0.5em;"><i class="fas fa-trash" style="margin-right: 0.5em;"></i>Supprimer</button> 
                            @endif
                            @if(auth()->user()->favorisTopic->find($topic->id_topic) != null)
                                <i class="fa fa-thumb-tack" aria-hidden="true" style="font-size: 25px;margin-left: 15px;"></i>
                            @endif
                        </div>
                    </div>
                    <div class="forumCategorieTopicTopic_titre" style="margin: 0.5em 1em 0 1em !important;font-size: 2em;">
                            {{$topic->titre}}
                    </div>
                    <div class='celluleDroiteTopicTopic' style="margin: 0 1em 0.5em 1em !important;">
                        Sujet crée par <strong style="margin-left:0.2em;">{{$topic->user->prenom}} {{$topic->user->nom}}</strong>
                    </div>
                </div>
                <div class="contentForumCategorieTopicTopic_description">
                    <div class='forumTopCategorieTopic' style="display: block;">
                        <div class='celluleGaucheTopicTopic' style="text-align: center; ">	
                           <strong style="font-size: 1.5em"> Description du sujet </strong>
                        </div>
                        <div style="text-overflow: ellipsis;overflow: hidden; width: auto; margin-right: 1em; margin-left: 1rem;">
                            <textarea readonly class="unfocus autoSize" style="resize:none;" class="autoSize">{{$topic->description}}</textarea>
                        </div>
                        <div class='celluleDroiteTopicTopic'>	
                            <strong>{{$topic->created_at}}</strong>
                        </div>
                    </div>
                </div>
            </div>
            @foreach ($lesReponses as $reponse)
            <div class="contentForumCategorieTopicTopic_description">
                <div class='forumTopCategorieTopic' style="display: block;">
                    <div class='celluleGaucheTopicTopic'>
                    Réponse de <strong>{{$reponse->user->prenom}} {{$reponse->user->nom}}</strong>
                    <div class='celluleDroiteTopicTopic'>
                    @if($reponse->user->id_utilisateur == auth()->user()->id_utilisateur)
                        <button id="btnModifier{{$reponse->id_reponse}}"type="button" onclick="modifierReponse({{$reponse->id_reponse}});" class="btn btnTopic btnEdit" style="margin-right: 1em"><i class="fas fa-pencil-alt" style="margin-right: 0.5em"></i>Modifier</button>
                        <button id="btnConfirmer{{$reponse->id_reponse}}"type="button" onclick="confirmer({{$reponse->id_reponse}})" class="btn btnTopic btnEdit" style="display: none; margin-right: 1em"><i class="fas fa-check" style="margin-right: 0.5em"></i>Confirmer</button>
                    @endif
                        <form method="POST" action="{{route('front.reponse.update', $reponse)}}" style="margin: 0" id="formEditReponse{{$reponse->id_reponse}}" style="display: none">
                                @csrf
                                @method('PATCH')
                                <textarea class='textAreaTopic' id='modifReponse{{$reponse->id_reponse}}'  style="display: none" name='modifReponse'></textarea>
                        </form>
                    @if(auth()->user()->Role->designation == 'admin' OR $reponse->user->id_utilisateur == auth()->user()->id_utilisateur)
                        <button  id="btnSupprimer{{$reponse->id_reponse}}" data-action='{{route('front.reponse.destroy', $reponse)}}' class="btn btnTopic btnDelete remove-reponse"><i class="fas fa-trash" style="margin-right: 0.5em;"></i>Supprimer</button>
                    @endif
                    @if($reponse->user->id_utilisateur == auth()->user()->id_utilisateur)
                        <button  id="btnAnnuler{{$reponse->id_reponse}}"type="button" onclick="annulerModification('{{$reponse->id_reponse}}', '{{$reponse->description}}');" class="btn btnTopic btnDelete" style="display: none"><i class="fas fa-times" style="margin-right: 0.5em"></i>Annuler</button>
                    @endif
                    </div> 
                </div>
                    <div style="text-overflow: ellipsis;overflow: hidden; width: auto; margin-right: 1em; margin-left: 1rem; ">
                    <textarea id="textAreaReponse{{$reponse->id_reponse}}"readonly class="unfocus autoSize" style="resize:none;"">{{$reponse->description}}</textarea>
                    </div>
                    <div class='celluleDroiteTopicTopic'>	
                        <strong>{{$reponse->created_at}}</strong>
                    </div>
                </div>
            </div>
            @endforeach     
            <div id='divReponse' style="display: none">
                <form method="POST" action="{{route('front.reponse.store', $topic)}}">
                    @csrf
                    @method('POST')
                    <textarea class='textAreaTopic' id='reponse' name='reponse'></textarea>
                    <button class="btn btn-primary" style="margin-top: 0.5em" type="submit">Envoyer</button>
                    <button class="btn btnSupprimer" style="margin-top: 0.5em" onClick="annuler();"type="button">Annuler</button>
                </form>
            </div>
            <!-- Bouton -->
            <div id='divBoutonRepondre'>
                <button class="btn btn-primary" onClick="addTextArea()">Répondre</button>
                @if($topic->user->id_utilisateur == auth()->user()->id_utilisateur)
                    <form method="POST" class="checkboxResolu" action="{{route('front.forum.resolu', $topic)}}" >
                        @csrf
                        @method('POST')

                        <input type="checkbox"  id="scales" name="scales" onchange="checkBoxResolu(this)">
                        <label style="margin-top: 0.5em;" class="textResolu" for="scales">Résolu</label>  
                        <input type="submit" id="envoieResolu" style="d">  
                    </form>
                @endif
            </div>
            @if(count($lesReponses->links()->elements[0]) > 1)
			<div class="pagination pagination-front">		
				<div class="pagination">
					{{$lesReponses->links()}}
				</div>
			</div>
            @endif
        </div>
    </section>
    @section('script')
    <script type="text/javascript">
        //Boutton submit invisible
        if({{$topic->user->id_utilisateur}} == {{auth()->user()->id_utilisateur}}){
            document.getElementById("envoieResolu").style.visibility= "hidden";

            if({{$topic->cloturer}} == 1){
                document.getElementById("scales").checked =true;
            }
        }        

        $("body").on("click", ".remove-reponse", function() {

            var current_object = $(this);
            swal.fire({

                title: "Êtes vous sur ?",
                text: "Etes vous sur de supprimer la réponse ? (action irréversible)",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#6c757d',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Supprimer',
                reverseButtons: true

            }).then((result) => {

                if (result.value) {

                    var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('DELETE')');
                    $('body').find('.remove-form').submit();
                }

            });

        });

        $("body").on("click", ".remove-topic", function() {

            var current_object = $(this);
            swal.fire({

                title: "Êtes vous sur ?",
                text: "Etes vous sur de supprimer ce topic ? (action irréversible)",
                type: 'warning',
                showCancelButton: true,
                cancelButtonColor: '#6c757d',
                confirmButtonColor: '#dc3545',
                confirmButtonText: 'Supprimer',
                reverseButtons: true

            }).then((result) => {

                if (result.value) {

                    var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('DELETE')');
                    $('body').find('.remove-form').submit();
                }

            });

        });

        function annuler(){
            document.getElementById("divReponse").style.display= "none";
            document.getElementById('divBoutonRepondre').style.display = "initial";
        }

        window.addEventListener('resize', function(){
            $( ".autoSize" ).each(function() {	
                $(this).height('1px');
                $(this).height((this.scrollHeight - 20)+"px");
            });
        });
        $( ".autoSize" ).each(function() {
            $(this).height('1px');
            $(this).height((this.scrollHeight - 20)+"px");
        });

        function confirmer(id){
            var txt = document.getElementById('textAreaReponse'+id).value;
            document.getElementById('modifReponse'+id).value = txt;
            document.getElementById('formEditReponse'+id).submit();
        }

        function annulerModification(id, txt){
            document.getElementById('textAreaReponse'+id).setAttribute('readonly', "");
            document.getElementById('textAreaReponse'+id).setAttribute('class', 'unfocus');
            document.getElementById('textAreaReponse'+id).value= txt;
            document.getElementById('btnModifier'+id).style.display = "";
            document.getElementById('btnSupprimer'+id).style.display = "";
            document.getElementById('btnConfirmer'+id).style.display = "none";
            document.getElementById('btnAnnuler'+id).style.display = "none";
        }

        function modifierReponse(id){
            console.log('tt');
            document.getElementById('textAreaReponse'+id).removeAttribute('readonly');
            document.getElementById('textAreaReponse'+id).removeAttribute('class', 'unfocus');
            document.getElementById('textAreaReponse'+id).select();
            document.getElementById('btnModifier'+id).style.display = "none";
            document.getElementById('btnSupprimer'+id).style.display = "none";
            document.getElementById('btnConfirmer'+id).style.display = "";
            document.getElementById('btnAnnuler'+id).style.display = "";
        }

        function addTextArea(text){
            //Supprimer le bouton 'Répondre'
            document.getElementById('divBoutonRepondre').style.display = "none";

            //Affichage de la div 'divReponse'
            document.getElementById('reponse').value ="";
            document.getElementById('divReponse').style.display = "initial";
            
            //Selection du text area
            document.getElementById('reponse').select();
        }
        
        function checkBoxResolu(element){
            var txt;
            var form = document.getElementById('envoieResolu');

            if (element.checked){
                swal.fire({
                    title: "Êtes vous sur ?",
                    text: "Voulez-vous passer le sujet en résolu ?",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: '#6c757d',
                    confirmButtonColor: '#dc3545',
                    confirmButtonText: 'Oui',
                    reverseButtons: true

                    }).then((result) => {

                    if (result.value) {
                        form.click();
                    }else{
                        element.checked = !element.checked;
                    }
                });
            }
            else{
                swal.fire({
                    title: "Êtes vous sur ?",
                    text: "Voulez-vous passer le sujet en non résolu ?",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonColor: '#6c757d',
                    confirmButtonColor: '#dc3545',
                    confirmButtonText: 'Oui',
                    reverseButtons: true

                    }).then((result) => {

                    if (result.value) {
                        form.click();
                    }else{
                        element = element.checked=true;
                    }
                });
            }
        } 
    </script>
    @endsection
@endsection


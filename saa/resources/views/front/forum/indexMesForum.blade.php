@extends('layouts.front')
@section('titleBanner')
Liste de mes sujets
@endsection

@section('content')
<section class="wrapper">
    <div class="inner">
            <div id='divBoutonRepondre' class="header-topic">
                <a href="{{route('front.categorie.index')}}"><button class="btn btn-primary">Retour</button></a>
                <div class="legende">
                    <span class='iconeTopicResolu'>
                        <img style="width: 18px !important;" src="{{asset('/storage/categorieLogo/icone-verte.jpg')}}" width="18">
                        Sujet résolu
                    </span>
                </div>

                <form method="POST" action="{{route('front.mesForum.search')}}" class="form-search-topic">
					@csrf
					@method('POST')

					<input type="text" name ="search" id="search" style="width: 20em;height: 2em;border-radius: 4px 0px 0px 4px;border-right: none;background-color: white;" placeholder="Rechercher ...">
					<button type="submit" class="btn icon-search-topic"><i class="fas fa-search"></i></button>
				</form>
            </div>
        <div class="highlights">
            @foreach ($lesTopics as $topic)
                <a class='aForumCategorie' href="{{route('front.forum.show', [$topic])}}">
                <div class="rotate">
                    <div class="contentForumCategorie">
                            <div class='forumTopCategorieTopic'>									
									@if($topic->cloturer == 1)
									<span class='iconeCategorie iconeTopicResolu' style="margin: 0">
										<img id="image" class='iconeCategorie iconeTopicResolu' style="margin: 0" src="{{asset('/storage/categorieLogo/icone-verte.jpg')}}" width="22">
									</span>
									@endif
									
									<div class="forumCategorieTopic_titre" >
										{{$topic->titre}}
                                    </div>
                                    
                                    @if(auth()->user()->favorisTopic->find($topic->id_topic) != null)
									<div class="celluleDroiteTopicTopic" >
										<i class="fa fa-thumb-tack" aria-hidden="true" style="font-size: 25px;margin-left: 15px;"></i>
                                    </div>
                                    @endif
							</div>
                            <div class="forumTopCategorieTopic">
                                <div class='celluleGaucheTopic'>	
                                    <span class='dernierMessage_lien'>
                                        Par
                                        <strong>{{$topic->user->prenom}} {{$topic->user->nom}}</strong>
                                        {{$topic->created_at}}
                                    </span>
                                </div>
                                <div class='celluleMilieuTopic'>
                                    {{count($topic->reponsesTopic)}} messages
                                </div>
                                <div class='celluleDroiteTopic'>
                                    <span class='dernierMessage_lien'>
                                        @if(count($topic->reponsesTopic) != 0)
                                            Dernier message par
                                            <strong>
                                                {{optional($topic->reponsesTopic->last())->user->prenom}} {{optional($topic->reponsesTopic->last())->user->nom}}
                                            </strong>
                                        @else
                                            <strong>Aucune réponse</strong>
                                        @endif
                                    </span>
                                </div>
                            </div>
                        <div class='forumBotCategorieTopic'>
                            <hr>
                            <strong >
                                <span class='dernierMessage'>Description :</span>
                            </strong>
                            <div class='dernierMessage_lien'>
                                {{$topic->description}}
                            </div>
                        </div>
                    </div>
                </div>
                </a>						
            @endforeach	
        </div>
        @if(count($lesTopics->links()->elements[0]) > 1)
		<div class="pagination" style="display: block ruby">		
			<div class="pagination">
				{{$lesTopics->links()}}
			</div>
        </div>
        @endif
    </div>
</section>
@endsection
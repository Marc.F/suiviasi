@extends('layouts.front')
@section('titleBanner', 'Favoris')

@section('content')
<section class="wrapper">
    <div class="inner">
    
        <div id='divBoutonRepondre'>
          <a href="{{route('home')}}"><button class="btn boutonOffrePrimary" style=""><i class="fas fa-home" style="margin-right: 1em"></i>Accueil</button></a>
          <a href="{{route('front.profil.index')}}"><button class="btn boutonOffrePrimary" style=""><i class="fas fa-user-cog" style="margin-right: 1em;"></i>Profil</button></a>
        </div>

        @if(count($lesTopics) == 0 && count($lesOffres) == 0))
        <div class="highlights" style="margin-bottom: 0;">
          <div class="contentForumCategorie" style="padding: 0 !important; width: auto;">
            <div style="margin: 0.5em 1em;font-style: italic;">
              <span style="color:red;"> Aucun favoris </span>
            </div>
          </div>
        </div>
        @endif
        
        @if(count($lesTopics) != 0)
        <div class="highlights" style="margin-bottom: 0em;">
          <div class="contentForumCategorie" style="padding: 0 !important; width: auto;">
            <div style="margin: 0.5em 1em;font-size: 1.5em;font-style: italic;text-decoration: underline;text-decoration-thickness: 0.1em;">
              <strong> Liste des forums en favoris </strong>
            </div>
          </div>
          @foreach ($lesTopics as $topic)
                <a class='aForumCategorie' href="{{route('front.forum.show', [$topic])}}">
                <div class="rotate">
                    <div class="contentForumCategorie">
                      <div class='forumTopCategorieTopic'>									
                        @if($topic->cloturer == 1)
                          <span class='iconeCategorie iconeTopicResolu' style="margin: 0">
                            <img id="image" class='iconeCategorie iconeTopicResolu' style="margin: 0" src="{{asset('/storage/categorieLogo/icone-verte.jpg')}}" width="22">
                          </span>
                        @endif
                        <div class="forumCategorieTopic_titre" >
                          {{$topic->titre}}
                        </div>
							        </div>
                      <div class="forumTopCategorieTopic">
                        <div class='celluleGaucheTopic'>	
                          <span class='dernierMessage_lien'>
                            Par
                            <strong>{{$topic->user->prenom}} {{$topic->user->nom}}</strong>
                            {{$topic->created_at}}
                          </span>
                        </div>
                        <div class='celluleMilieuTopic'>
                          {{count($topic->reponsesTopic)}} messages
                        </div>
                        <div class='celluleDroiteTopic'>
                          <span class='dernierMessage_lien'>
                            @if(count($topic->reponsesTopic) != 0)
                              Dernier message par
                              <strong>
                                {{optional($topic->reponsesTopic->last())->user->prenom}} {{optional($topic->reponsesTopic->last())->user->nom}}
                              </strong>
                            @else
                              <strong>Aucune réponse</strong>
                            @endif
                          </span>
                        </div>
                      </div>
                      <div class='forumBotCategorieTopic'>
                        <hr>
                        <strong >
                          <span class='dernierMessage'>Description :</span>
                        </strong>
                        <div class='dernierMessage_lien'>
                          {{$topic->description}}
                        </div>
                      </div>
                    </div>
                </div>
                </a>						
          @endforeach	            
        </div>
        @endif

        @if(count($lesTopics->links()->elements[0]) > 1)
        <div class="pagination" style="display: block ruby">		
          <div class="pagination">
            {{$lesTopics->appends(['lesOffres' => $lesOffres->currentPage()])->links()}}
          </div>
        </div>
        @endif
        
        @if(count($lesOffres) != 0)
        <div class="highlights" style="margin-bottom: 0;">
          <div class="contentForumCategorie" style="padding: 0 !important; width: auto;">
            <div style="margin: 0.5em 1em;font-size: 1.5em;font-style: italic;text-decoration: underline;text-decoration-thickness: 0.1em;">
              <strong> Liste des offres en favoris </strong>
            </div>
          </div>
        </div>

        <div class="highlights" style="margin-top: 0; margin-bottom: 1em; ">
          @foreach ($lesOffres as $offre)
						<section>
							<a href="{{route('front.offre.show', $offre)}}">
							<div class="rotate">
									<div class="content">
											<header>
												<span class="icon fa-vcard-o"></span>
												<span class='dernierMessage_lien'>
													<h3 class='cardTitle overflowHide'>{{$offre->titre}}</h3>
												</span>
											</header>
											<span class='dernierMessage_lien'><p class='cardIntituler overflowHide'>Entreprise : <span class='cardName'>{{$offre->entreprise}}</span></p></span>
											<span class='dernierMessage_lien'><p class='cardIntituler overflowHide'>Ville : <span class='cardName'>{{$offre->ville}}</span></p></span>
											<span class='dernierMessage_lien'><p class='cardIntituler overflowHide'>Salaire : <span class='cardName'>{{$offre->salaire}}</span></p></span>
											<span class='dernierMessage_lien'><p class='cardIntituler overflowHide'>Niveau Etude : <span class='cardName'>{{$offre->niveau_etude}}</span></p></span>
									</div>
							</div>
							</a>						
						</section>
					@endforeach  
        </div>
        @endif

        @if(count($lesOffres->links()->elements[0]) > 1)
        <div class="pagination" style="display: block ruby">		
          <div class="pagination">
            {{$lesOffres->appends(['lesTopics' => $lesTopics->currentPage()])->links()}}
          </div>
        </div>
        @endif
      </div>
    </div>
  </section>
@endsection
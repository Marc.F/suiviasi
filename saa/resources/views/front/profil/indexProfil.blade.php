@extends('layouts.front')
@section('titleBanner', 'Profil')

@section('link')
<link rel="stylesheet" href="{{asset('adminlte/css/adminlte.css') }}" />
@endsection

@section('content')
<section class="wrapper">
  <div class="inner">
        <div id='divBoutonRepondre'>
            <a href="{{route('home')}}"><button class="btn boutonOffrePrimary" style=""><i class="fas fa-home" style="margin-right: 1em"></i>Accueil</button></a>
            <a href="{{route('front.profil.favoris')}}"><button class="btn boutonOffrePrimary" style=""><i class="fas fa-heart" style="margin-right: 1em; color: red;"></i>Favoris</button></a>
        </div>
        <div class="highlights">
            <div class="wrap-login100" style="width: 100%; padding: 0">
                <div class="content_card">
                    <div class="card-header">
                        <form method="POST" action="{{route('front.profil.update')}}">
                            @csrf
                            @method('PUT')
                            <div style="display: flex; margin : 1em">
                                <input type="checkbox" id="infoBox" name="infoBox"onclick="change_info();" >
                                <label for="infoBox">Changer les informations personnelles</label><br>
                                <input type="checkbox" id="mdpBox" name="mdpBox" onclick="change_mdp();" style="margin-left: 1em">
                                <label for="mdpBox">Changer de mot de passe</label><br>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nom</label>
                                        <input type="text" class="form-control @error('nom') is-invalid @enderror" id="nom" name="nom"
                                            placeholder="Entrer un nom..." value="@if (old('nom')) {{old('nom')}} @else{{auth()->user()->nom}} @endif" disabled>
                                        @error('nom') <span id="name-error"
                                            class="error invalid-feedback">{{ $errors->first('nom') }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Prenom</label>
                                        <input type="text" class="form-control @error('prenom') is-invalid @enderror" id="prenom"
                                            name="prenom" placeholder="Entrer un prenom..." value="@if (old('prenom')) {{old('prenom')}} @else{{auth()->user()->prenom}} @endif" disabled>
                                        @error('prenom') <span id="name-error"
                                            class="error invalid-feedback">{{ $errors->first('prenom') }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="text" class="form-control @error('email') is-invalid @enderror" id="email"
                                            name="email" placeholder="Entrer un email..." value="@if (old('email')) {{old('email')}} @else{{auth()->user()->email}} @endif" disabled>
                                        @error('email') <span id="name-error"
                                            class="error invalid-feedback">{{ $errors->first('email') }}</span> @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Telephone</label>
                                        <input type="text" class="form-control @error('telephone') is-invalid @enderror" id="telephone"
                                            name="telephone" placeholder="Entrer un telephone..." value="@if (old('telephone')) {{old('telephone')}} @else{{auth()->user()->telephone}} @endif" disabled>
                                        @error('telephone') <span id="name-error"
                                            class="error invalid-feedback">{{ $errors->first('telephone') }}</span> @enderror
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Rôle</label>
                                        <input type="text" class="form-control" value="{{auth()->user()->role->designation}}" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="labelPassword">Mot de passe</label>
                                        <input type="password" class="form-control @error('password') is-invalid @enderror"
                                            id="password" name="password" placeholder="Entrer un mot de passe..." disabled>
                                        @error('password') <span id="name-error"
                                            class="error invalid-feedback">{{ $errors->first('password') }}</span> @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 0" id="newMDP" hidden>                       
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Nouveau mot de passe</label>
                                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                                id="newPassword" name="newPassword" placeholder="Entrer un mot de passe...">
                                            @error('password') <span id="name-error"
                                                class="error invalid-feedback">{{ $errors->first('password') }}</span> @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label id="">Confirmer le nouveau mot de passe</label>
                                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                                id="newPasswordConfirmation" name="newPasswordConfirmation"
                                                placeholder="Confirmer le mot de passe...">
                                        </div>
                                    </div>
                            </div>
                        <div  style="margin-top:1em">
                            <a style="display:block; color: #2C75FF;" class="desactive-account" data-action="{{ route('front.profil.desactive') }}" onmouseover="this.style.color='#FF0000';this.style.cursor='pointer';" onmouseout="this.style.color='#2C75FF';">
                                <span> Désactiver le compte </span>
                            </a>
                        </div>
                        <div style="margin-top:0.5em">
                            <a style="color: #2C75FF;" class="remove-account" data-action="{{ route('front.profil.delete') }}" onmouseover="this.style.color='#FF0000';this.style.cursor='pointer';" onmouseout="this.style.color='#2C75FF';">
                                <span> Supprimer le compte </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      <div id='divBoutonRepondre'>
        <button type="submit" id="btnProfil" class="btn boutonOffrePrimary" style="" hidden/></a>
      </div>
    </form>
  </div>
</section>
@section('script')
    <script type="text/javascript">
    window.addEventListener('resize', function(){
            $( ".autoSize" ).each(function() {	
                $(this).height('1px');
                $(this).height((this.scrollHeight - 20)+"px");
            });
        });
        $( ".autoSize" ).each(function() {
            $(this).height('1px');
            $(this).height((this.scrollHeight - 20)+"px");
    });

    function change_info(){
        if (document.getElementById("infoBox").checked == true) {
            document.getElementById("nom").disabled = false;
            document.getElementById("prenom").disabled = false;
            document.getElementById("email").disabled = false;
            document.getElementById("telephone").disabled = false;
            document.getElementById("password").disabled = false;
            document.getElementById("mdpBox").checked = false;
            
            document.getElementById("btnProfil").hidden = false;
            document.getElementById("btnProfil").innerText = "Mettre à jour les informations";

            change_mdp();
        }        
        else{
            document.getElementById("nom").disabled = true;
            document.getElementById("prenom").disabled = true;
            document.getElementById("email").disabled = true;
            document.getElementById("telephone").disabled = true;
        }

        if(document.getElementById("infoBox").checked == false && document.getElementById("mdpBox").checked == false){
            document.getElementById("btnProfil").hidden = true;
            document.getElementById("password").disabled = true;
        }
    }

    function change_mdp() {
        if (document.getElementById("mdpBox").checked == true) {
            document.getElementById("password").disabled = false;
            document.getElementById("infoBox").checked = false;
            document.getElementById("newMDP").hidden = false;
            document.getElementById("labelPassword").innerText = "Ancien mot de passe";

            document.getElementById("btnProfil").hidden = false;
            document.getElementById("btnProfil").innerText = "Réinitialiser le mot de passe";

            change_info();
        }else{
            document.getElementById("labelPassword").innerText = "Mot de passe"
            document.getElementById("newMDP").hidden = true;
        }

        if(document.getElementById("infoBox").checked == false && document.getElementById("mdpBox").checked == false){
            document.getElementById("btnProfil").hidden = true;
            document.getElementById("password").disabled = true;
        }
    }

    $("body").on("click", ".remove-account", function() {

        var current_object = $(this);

        swal.fire({

            title: "Êtes vous sur ?",
            text: "Voulez-vous vraiment supprimer votre compte ?",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#6c757d',
            confirmButtonColor: '#dc3545',
            confirmButtonText: 'Supprimer !',
            reverseButtons: true

        }).then((result) => {

            if (result.value) {
                    var action = current_object.attr('data-action');
                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('DELETE')');
                    $('body').find('.remove-form').submit();
                
            }

        });

    });

    $("body").on("click", ".desactive-account", function() {

        var current_object = $(this);

        swal.fire({

            title: "Êtes vous sur ?",
            text: "Voulez-vous vraiment désactiver votre compte ?",
            type: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#6c757d',
            confirmButtonColor: '#dc3545',
            confirmButtonText: 'Désactiver !',
            reverseButtons: true

        }).then((result) => {

            if (result.value) {
                var action = current_object.attr('data-action');

                    $('body').html("<form class='form-inline remove-form' method='post' action='" + action +
                        "'></form>");
                    $('body').find('.remove-form').append('@csrf');
                    $('body').find('.remove-form').append('@method('PUT')');
                    $('body').find('.remove-form').submit();
            }

        });

    });

    </script>
@endsection
@endsection

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>SAA Pasteur Mont-Roland</title>

  <link rel="icon" type="image/png" href="{{asset('front/images/favicon.ico') }}">
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('adminlte/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{asset('adminlte/css/back.css') }}">

  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('adminlte/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">


</head>
<body class="hold-transition sidebar-mini">
  @include('sweetalert::alert')
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
      <a href="{{route('home')}}" class="nav-link">Accueil</a>
      </li>
    </ul>
  <!-- Fin Navbar -->




    <ul class="navbar-nav ml-auto">
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{route('admin')}}" class="nav-link">Panneau d'administration</a>
        </li>
    </ul>
  
  </nav>
  <!-- /.navbar -->




  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="brand-link">
      <img src={{ asset('front/images/icon_saa.png') }} class="img-fluid rounded mx-auto d-block" style="border-radius: 5rem!important"/>
    </div>
    <!-- Sidebar -->
    <div class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-transition os-host-scrollbar-horizontal-hidden">
    
      <!-- Sidebar Menu -->

      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('offre.index')}}" class="nav-link {{ request()->routeIs('offre.*') ? 'active' : ''}}">
              <i class="nav-icon fas fa-comments"></i>
              <p>
                Offres
              </p>
            </a>
        </ul>

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('forum.index')}}" class="nav-link {{ request()->routeIs('forum.*') ? 'active' : ''}}">
              <i class="nav-icon fas fa-book-reader"></i>
              <p>
                Forum
              </p>
            </a>
        </ul>

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('questionnaire.index')}}" class="nav-link {{ request()->routeIs('questionnaire.*') ? 'active' : ''}}">
              <i class="nav-icon fas fa-question-circle"></i>
              <p>
                Questionnaire
              </p>
            </a>
        </ul>

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('user.index')}}" class="nav-link {{ request()->routeIs('user.*') ? 'active' : ''}}">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Utilisateurs
              </p>
            </a>
        </ul>

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('role.index')}}" class="nav-link {{ request()->routeIs('role.*') ? 'active' : ''}}">
              <i class="nav-icon fas fa-key"></i>
              <p>
                Roles
              </p>
            </a>
          </ul>   

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{route('categorie.index')}}" class="nav-link {{ request()->routeIs('categorie.*') ? 'active' : ''}}">
              <i class="nav-icon fas fa-grip-horizontal"></i>
              <p>
                Catégories
              </p>
            </a>
        </ul>

       
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->



    
  </aside>

  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="min-height: 823px;">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@yield('title')</h1>
          </div><!-- /.col -->

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="content">
      <div class="container-fluid"> 
          @yield('content')
      </div>
    </div>

    
  </div>
  <!-- /.content-wrapper -->


</div>

<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery 3 -->
<script src="https://code.jquery.com/jquery-3.2.0.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminlte/js/adminlte.min.js') }}"></script>

<!-- DataTables -->
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{asset('adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{asset('adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>

<!-- SweetAlert -->
<script src="{{ asset('adminlte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

<!-- Select2 -->
<script src="{{asset('adminlte/plugins/select2/js/select2.full.min.js') }}"> </script>

@yield('script')


</body>
</html>

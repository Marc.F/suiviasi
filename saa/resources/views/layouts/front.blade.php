<!DOCTYPE HTML>
<!--
	Industrious by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>SAA Pasteur Mont-Roland</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="icon" type="image/png" href="{{asset('front/images/favicon.ico') }}">
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link rel="stylesheet" href={{asset('front/css/main.css') }} />
		<link rel="stylesheet" href={{asset('front/css/card.css') }} />
		<link rel="stylesheet" href="{{asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
		@yield('link')
		
		<!-- Select2 -->
		<link rel="stylesheet" href="{{asset('adminlte/plugins/select2/css/select2.min.css') }}">
		<link rel="stylesheet" href="{{asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

		<link rel="stylesheet" href="{{ asset('adminlte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css') }}">

	</head>
	<body class="is-preload">
		@include('sweetalert::alert')
		<!-- Header -->
			<header id="header">
				<img  class="logo" src="{{asset('front/images/logo.png')}}"/>
				<nav>
					<i class="fas fa-bell btn-notif" id="notifBtn"><span class="span-notif" id="spanNotif"><p class="span-notif-txt" id="notif"></p></span></i>
					<div class="dropdown-content dropdown-content-empty" id="dropdown" style="display:none">
						<div id="message">
							
						</div>
						<div id="emptyMessage" class="empty-message">
							Aucune Notification ! 
						</div>
						<div>
							<button id="deleteNotifBtn" class="btn-notif-suppr"> <i class="fas fa-trash" style="margin-right: 10px;"></i>Supprimer les notifications</button>
						</div>
					</div>
					<a href="#menu">Menu</a>
				</nav>
			</header>
			<div>
						
			</div>
		<!-- Nav -->
			<nav id="menu" style="display: inline-table">
				<ul class="links">
					<li><a href="{{route('home')}}" style="line-height: inherit; padding: 1em 0em;"><i class="fas fa-home fa-lg" style="padding-right: 1em"></i>Accueil</a></li>
					<li><a href="{{route('front.offre.index')}}" style="line-height: inherit; padding: 1em 0em;"><i class="fas fa-book-reader fa-lg" style="padding-right: 1em"></i>Offre d'emploi</a></li>
					<li><a href="{{route('front.categorie.index')}}" style="line-height: inherit; padding: 1em 0em;"><i class="fas fa-comments fa-lg" style="padding-right: 1em"></i>Forum</a></li>
					<li><a href="{{route("front.questionnaire.index")}}" style="line-height: inherit; padding: 1em 0em;"><i class="fas fa-question-circle fa-lg" style="padding-right: 1em"></i>Questionnaire</a></li>
					<li><a href="{{route('front.profil.index')}}" style="line-height: inherit; padding: 1em 0em;"><i class="fas fa-user-cog fa-lg" style="padding-right: 1em"></i>Mon profil</a></li>
					@if(auth()->user()->role->designation == "admin")
					<li><a href="{{route('admin')}}" style="line-height: inherit; padding: 1em 0em;"><i class="fas fa-tools fa-lg" style="padding-right: 1em;"></i>Panneau d'administration</a></li>
					@endif
				</ul>
				<div style="padding :1em 1em">
				<form action="{{ route('logout') }}" method="POST" style="bottom: 0; margin: 0em">
					@csrf
					<button type="submit" class="btn" style="display: block ruby; width: 100%"><i class="fas fa-power-off fa-lg"></i> <div style="padding: 0 1em;" >Deconnexion</div></button>
				</form>
				</div>
			</nav>

		<!-- Banner -->
			<section id="banner">
				<div class="inner">
					<h1>@yield('titleBanner')</h1>
				</div>
			</section>

			<main class="contentLayout" id="contentLayout">
				@yield('content')
			</main>

		<!-- Highlights
			<section class="wrapper">
				<div class="inner">
					<header class="special">
						<h2>Sem turpis amet semper</h2>
						<p>In arcu accumsan arcu adipiscing accumsan orci ac. Felis id enim aliquet. Accumsan ac integer lobortis commodo ornare aliquet accumsan erat tempus amet porttitor.</p>
					</header>
					<div class="highlights">
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-vcard-o"><span class="label">Icon</span></a>
									<h3>Feugiat consequat</h3>
								</header>
								<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
							</div>
						</section>
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-files-o"><span class="label">Icon</span></a>
									<h3>Ante sem integer</h3>
								</header>
								<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
							</div>
						</section>
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-floppy-o"><span class="label">Icon</span></a>
									<h3>Ipsum consequat</h3>
								</header>
								<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
							</div>
						</section>
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-line-chart"><span class="label">Icon</span></a>
									<h3>Interdum gravida</h3>
								</header>
								<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
							</div>
						</section>
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-paper-plane-o"><span class="label">Icon</span></a>
									<h3>Faucibus consequat</h3>
								</header>
								<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
							</div>
						</section>
						<section>
							<div class="content">
								<header>
									<a href="#" class="icon fa-qrcode"><span class="label">Icon</span></a>
									<h3>Accumsan viverra</h3>
								</header>
								<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
							</div>
						</section>
					</div>
				</div>
			</section>

		CTA
			<section id="cta" class="wrapper">
				<div class="inner">
					<h2>Curabitur ullamcorper ultricies</h2>
					<p>Nunc lacinia ante nunc ac lobortis. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus ornare mi ut ante amet placerat aliquet. Volutpat eu sed ante lacinia sapien lorem accumsan varius montes viverra nibh in adipiscing. Lorem ipsum dolor vestibulum ante ipsum primis in faucibus vestibulum. Blandit adipiscing eu felis iaculis volutpat ac adipiscing sed feugiat eu faucibus. Integer ac sed amet praesent. Nunc lacinia ante nunc ac gravida.</p>
				</div>
			</section>

		Testimonials
			<section class="wrapper">
				<div class="inner">
					<header class="special">
						<h2>Faucibus consequat lorem</h2>
						<p>In arcu accumsan arcu adipiscing accumsan orci ac. Felis id enim aliquet. Accumsan ac integer lobortis commodo ornare aliquet accumsan erat tempus amet porttitor.</p>
					</header>
					<div class="testimonials">
						<section>
							<div class="content">
								<blockquote>
									<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
								</blockquote>
								<div class="author">
									<div class="image">
										<img src="images/pic01.jpg" alt="" />
									</div>
									<p class="credit">- <strong>Jane Doe</strong> <span>CEO - ABC Inc.</span></p>
								</div>
							</div>
						</section>
						<section>
							<div class="content">
								<blockquote>
									<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
								</blockquote>
								<div class="author">
									<div class="image">
										<img src="images/pic03.jpg" alt="" />
									</div>
									<p class="credit">- <strong>John Doe</strong> <span>CEO - ABC Inc.</span></p>
								</div>
							</div>
						</section>
						<section>
							<div class="content">
								<blockquote>
									<p>Nunc lacinia ante nunc ac lobortis ipsum. Interdum adipiscing gravida odio porttitor sem non mi integer non faucibus.</p>
								</blockquote>
								<div class="author">
									<div class="image">
										<img src="images/pic02.jpg" alt="" />
									</div>
									<p class="credit">- <strong>Janet Smith</strong> <span>CEO - ABC Inc.</span></p>
								</div>
							</div>
						</section>
					</div>
				</div>
			</section> -->



		<!-- Footer -->
			<footer id="footer">
				<div class="inner">
					<div class="content">
						<section>
							<h4><i class="fas fa-users fa-lg" style="padding-right: 1em;"></i>TEAM</h4>
							<ul class="alt">
								<li>FRANCESCHI Marc</li>
								<li>OUTHIER Loïc</li>
								<li>LABRUNIE Arthur</li>
								<li>MAZOYER Geoffrey</li>
							</ul>
						</section>
					</div>
					<div class="copyright">
						<p>Lycée Pasteur Mont-Roland</p>
					</div>
				</div>
			</footer>

		<!-- Scripts -->
			
			<script src={{ asset('front/js/jquery.min.js')}}></script>
			<script src={{ asset('front/js/browser.min.js')}}></script>
			<script src={{ asset('front/js/breakpoints.min.js')}}></script>
			<script src={{ asset('front/js/util.js')}}></script>
			<script src={{ asset('front/js/main.js')}}></script>
			<!-- SweetAlert -->
			<script src="{{ asset('adminlte/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
			<script src={{asset('front/js/notif.js')}}></script>
		

			@yield('script')
	</body>

</html>

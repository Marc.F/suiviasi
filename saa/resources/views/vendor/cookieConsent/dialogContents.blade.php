<div class="modal" style="display: flex" id="js-cookie-consent">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <span class="cookie-consent__message">
                    {!! trans('cookieConsent::texts.message') !!}
                </span>

            </div>

            <div class="modal-footer">
                <button class="btn btn-success" id="js-cookie-consent-agree">
                    {{ trans('cookieConsent::texts.agree') }}
                </button>
            </div>
        </div>
    </div>
</div>

@extends('layouts.front')

@section('titleBanner', 'Bienvenue')

@section('content')
			<section class="wrapper">
				<div class="inner">
					<div class="highlights">
						<section class='sizeHome'>
                            <a href="{{route('front.offre.index')}}">
                                <div class="rotate">
                                    <div class="contentOffre">
                                        <div class="content">
                                                <header>
                                                    <span class="fas fa-book-reader fa-lg" style="font-size: 100px; margin-bottom: 40px"></span>
                                                    <span class='dernierMessage_lien'>
                                                        <h3 class='cardTitle' style="font-size: 25px; color: black"> Liste d'emploi</h3>
                                                    </span>
                                                </header>
                                                <p class='cardIntituler' style="font-size: 20px">Dernières offres : </p>
                                                @foreach ($lesOffres as $offre)
                                                    <span class='dernierMessage_lien'><p class='cardIntituler overflowHide'><span class='cardName'>{{$offre->titre}}</span></p></span>
                                                @endforeach
                                        </div>
                                    </div>
                                </div>
                                </a>
						</section>
						<section class='sizeHome'>
							<a href="{{route('front.categorie.index')}}">
                                <div class="rotate">
                                    <div class="contentOffre">
                                        <div class="content">
                                                <header>
                                                    <span class="fas fa-comments fa-lg" style="font-size: 100px; margin-bottom: 40px"></span>
                                                    <span class='dernierMessage_lien'>
                                                        <h3 class='cardTitle' style="font-size: 25px; color: black">Forums</h3>
                                                    </span>
                                                </header>
                                                <p class='cardIntituler' style="font-size: 20px">Derniers forums : </p>
                                                @foreach ($lesTopics as $topic)
                                                    <span class='dernierMessage_lien'><p class='cardIntituler overflowHide'><span class='cardName'>{{$topic->titre}}</span></p></span>
                                                @endforeach
                                        </div>
                                    </div>
                                </div>
                                </a>
                        </section>
                        @if(auth()->user()->role->designation != "etudiant")
						<section class='sizeHome'>
							<a href="{{route("front.questionnaire.index")}}">
                                <div class="rotate">
                                    <div class="contentOffre">
                                        <div class="content">
                                                <header>
                                                    <span class="fas fa-question-circle fa-lg" style="font-size: 100px; margin-bottom: 40px"></span>
                                                    <span class='dernierMessage_lien'>
                                                        <h3 class='cardTitle' style="font-size: 25px; color: black">Questionnaires</h3>
                                                    </span>
                                                </header>
                                                <p class='cardIntituler' style="font-size: 20px">Derniers questionnaires : </p>
                                                @foreach ($lesQuestionnaires as $questionnaire)
                                                    <span class='dernierMessage_lien'><p class='cardIntituler overflowHide'><span class='cardName'>{{$questionnaire->titre}}</span></p></span>
                                                @endforeach
                                        </div>
                                    </div>
                                </div>
                                </a>
                        </section>
                        @endif
					</div>
				</div>
            </section>
            @section('script')
            <script type="text/javascript">
                $( ".sizeHome" ).each(function() {	
                   var x = $(this).width() * 2 
                });
            </script>
            @endsection
@endsection
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReponseTopic extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reponse_topics', function (Blueprint $table) {
            $table->id('id_reponse');
            $table->unsignedBigInteger('topic_id');
            $table->unsignedBigInteger('utilisateur_id');
            $table->longText('description');
            $table->timestamps();

            $table->foreign('topic_id')->references('id_topic')->on('topics')->onDelete('cascade');
            $table->foreign('utilisateur_id')->references('id_utilisateur')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

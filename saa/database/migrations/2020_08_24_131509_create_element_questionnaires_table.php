<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateElementQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('element_questionnaires', function (Blueprint $table) {
            $table->id('id_element');
            $table->string('description');
            $table->unsignedBigInteger('questionnaire_id');
            $table->timestamps();

            $table->foreign('questionnaire_id')->references('id_questionnaire')->on('questionnaires')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('element_questionnaires');
    }
}

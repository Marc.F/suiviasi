<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offres', function (Blueprint $table) {
            $table->id('id_offre');
            $table->string('titre');
            $table->longText('description');
            $table->string('adresse');
            $table->string('ville');
            $table->string('code_postal');
            $table->string('mail');
            $table->integer('telephone');
            $table->string('entreprise');
            $table->string('salaire');
            $table->string('niveau_etude');
            $table->unsignedBigInteger('utilisateur_id');
            $table->timestamps();

            //Ajout de la clé étrangère
            $table->foreign('utilisateur_id')->references('id_utilisateur')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offres');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReponseElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reponse_elements', function (Blueprint $table) {
            $table->id('id_reponse');
            $table->string('description');
            $table->unsignedBigInteger('element_id');
            $table->unsignedBigInteger('utilisateur_id');
            $table->timestamps();

            $table->foreign('element_id')->references('id_element')->on('element_questionnaires')->onDelete('cascade');
            $table->foreign('utilisateur_id')->references('id_utilisateur')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reponse_elements');
    }
}

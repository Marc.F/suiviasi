<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->id('id_topic');
            $table->string('titre');
            $table->longText('description');
            $table->boolean('cloturer');
            $table->unsignedBigInteger('utilisateur_id');
            $table->unsignedBigInteger('categorie_id');
            $table->timestamps();

            //ajout de la clé étrangère
            $table->foreign('utilisateur_id')->references('id_utilisateur')->on('users')->onDelete('cascade');;
            $table->foreign('categorie_id')->references('id_categorie')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}

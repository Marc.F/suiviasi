<?php

use Illuminate\Database\Seeder;

class TopicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('topics')->insert([
            'titre' => 'Pourquoi mon CSS fonctionne pas ?',
            'description' => 'Mon css ne fonctionne pas du tout, mon code : ..............................',
            'cloturer' => 0,
            'utilisateur_id' => 1,
            'categorie_id' => 1,
            'created_at' => '2020-09-23 15:18:20'
        ]);

        DB::table('topics')->insert([
            'titre' => 'Pourquoi mon HTML ne s\'affiche pas correctement ?',
            'description' => 'Mon ne s\'affiche pas du tout, mon code : ..............................',
            'cloturer' => 0,
            'utilisateur_id' => 1,
            'categorie_id' => 1,
            'created_at' => '2020-09-21 15:18:20'
        ]);

        DB::table('topics')->insert([
            'titre' => 'Erreur d\'affichage dans laravel',
            'description' => 'Ma page ne renvoie pas mes données, mon code : ..............................',
            'cloturer' => 1,
            'utilisateur_id' => 1,
            'categorie_id' => 1,
            'created_at' => '2020-09-16 15:18:20'
        ]);

        DB::table('topics')->insert([
            'titre' => 'Erreur d\'affichage en javascript',
            'description' => 'Mon bouton ne creer pas de zone de text, mon code : ..............................',
            'cloturer' => 0,
            'utilisateur_id' => 1,
            'categorie_id' => 2,
            'created_at' => '2020-09-23 15:18:20'
        ]);

        DB::table('topics')->insert([
            'titre' => 'Probleme de servlet en JEE',
            'description' => 'Je n\'arrive pas à accéder à ma servlet, mon code : ..............................',
            'cloturer' => 0,
            'utilisateur_id' => 1,
            'categorie_id' => 4,
            'created_at' => '2020-09-23 15:18:20'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class OffreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('offres')->insert([
            'titre' => 'Administrateur Système',
            'description' => 'Description ..............................................',
            'adresse' => '24 rue pierre',
            'ville' => 'Dijon',
            'code_postal' => '21000',
            'mail' => 'toto@gmail.com',
            'telephone' => '0692316529',
            'entreprise' => 'Toto',
            'salaire' => '2000/mois',
            'niveau_etude' => 'Bac+3',
            'utilisateur_id' => 1,
            'created_at' => '2020-09-21 15:18:20',
        ]);

        DB::table('offres')->insert([
            'titre' => 'Administrateur Système et Reseau',
            'description' => 'Description ..............................................',
            'adresse' => '25 rue quantin',
            'ville' => 'Besançon',
            'code_postal' => '25000',
            'mail' => 'marcel@gmail.com',
            'telephone' => '0692327023',
            'entreprise' => 'Marcel',
            'salaire' => '2500/mois',
            'niveau_etude' => 'Bac+4',
            'utilisateur_id' => 1,
            'created_at' => '2020-09-24 15:18:20',
        ]);

        DB::table('offres')->insert([
            'titre' => 'Developpeur JEE',
            'description' => 'Description ..............................................',
            'adresse' => '25 rue quantin',
            'ville' => 'Besançon',
            'code_postal' => '25000',
            'mail' => 'marcel@gmail.com',
            'telephone' => '0692327023',
            'entreprise' => 'Marcel',
            'salaire' => '2800/mois',
            'niveau_etude' => 'Bac+4',
            'utilisateur_id' => 1,
            'created_at' => '2020-09-15 15:18:20',
        ]);

        DB::table('offres')->insert([
            'titre' => 'Administrateur Reseau',
            'description' => 'Description ..............................................',
            'adresse' => '12 rue beacle',
            'ville' => 'Langres',
            'code_postal' => '52000',
            'mail' => 'spart@gmail.com',
            'telephone' => '0692562356',
            'entreprise' => 'Spart',
            'salaire' => '3000/mois',
            'niveau_etude' => 'Bac+5',
            'utilisateur_id' => 1,
            'created_at' => '2020-09-23 15:18:20',
        ]);
    }
}

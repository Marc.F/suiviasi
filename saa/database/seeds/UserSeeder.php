<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'nom' => 'OUTHIER',
            'prenom' => 'Loïc',
            'telephone' => '0771269548',
            'email' => 'loic.outhier.work@gmail.com',
            'password' => Hash::make('password'),
            'password_changed' => true,
            'active'=> true,
            'api_token' => Str::random(60),
            'role_id' => 1
        ]);
        DB::table('users')->insert([
            'nom' => 'ETUDIANT',
            'prenom' => 'etudiant',
            'telephone' => '0000000000',
            'email' => 'etudiant@gmail.com',
            'password' => Hash::make('password'),
            'password_changed' => false,
            'active'=> true,
            'api_token' => Str::random(60),
            'role_id' => 3
        ]);
        DB::table('users')->insert([
            'nom' => 'ANCIENETUDIANT',
            'prenom' => 'ancien_etudiant',
            'telephone' => '0000000000',
            'email' => 'ancienEtudiant@gmail.com',
            'password' => Hash::make('password'),
            'password_changed' => false,
            'active'=> true,
            'api_token' => Str::random(60),
            'role_id' => 2
        ]);
        DB::table('users')->insert([
            'nom' => 'RESPONSABLE',
            'prenom' => 'responsable',
            'telephone' => '0000000000',
            'email' => 'responsable@gmail.com',
            'password' => Hash::make('password'),
            'password_changed' => false,
            'active'=> true,
            'api_token' => Str::random(60),
            'role_id' => 4
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'designation' => 'admin',
        ]);
        DB::table('roles')->insert([
            'designation' => 'ancien_etudiant',
        ]);
        DB::table('roles')->insert([
            'designation' => 'etudiant',
        ]);
        DB::table('roles')->insert([
            'designation' => 'responsable',
        ]);
    }
}

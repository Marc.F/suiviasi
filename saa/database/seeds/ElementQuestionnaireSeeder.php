<?php

use Illuminate\Database\Seeder;

class ElementQuestionnaireSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('element_questionnaires')->insert([
            'description' => 'Nom',
            'questionnaire_id' => 1,
        ]);
        DB::table('element_questionnaires')->insert([
            'description' => 'Prenom',
            'questionnaire_id' => 1,
        ]);
        DB::table('element_questionnaires')->insert([
            'description' => 'Age',
            'questionnaire_id' => 1,
        ]);
        DB::table('element_questionnaires')->insert([
            'description' => 'Sexe',
            'questionnaire_id' => 1,
        ]);
    }
}
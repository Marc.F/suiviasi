<?php

use Illuminate\Database\Seeder;

class CategorieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'titre_categorie' => 'HTML-CSS',
            'description_categorie' => 'Un problème avec HTML ou CSS ? Venez demandez de l\'aide !',
            'logo_categorie' => '/categorieLogo/htmlIcone.png',
        ]);

        DB::table('categories')->insert([
            'titre_categorie' => 'Javascript',
            'description_categorie' => 'Une question à propos de Javascript.',
            'logo_categorie' => '/categorieLogo/javascriptIcone.png',
        ]);

        DB::table('categories')->insert([
            'titre_categorie' => 'PHP',
            'description_categorie' => 'Une question sur l\'utilisation du PHP ?',
            'logo_categorie' => '/categorieLogo/phpIcone.png',
        ]);

        DB::table('categories')->insert([
            'titre_categorie' => 'Langage-Java',
            'description_categorie' => 'Toutes vos questions sur le langage JAVA.',
            'logo_categorie' => '/categorieLogo/javaIcone.png',
        ]);

        DB::table('categories')->insert([
            'titre_categorie' => 'Langage-Python',
            'description_categorie' => 'Toutes vos questions sur le langage PYTHON.',
            'logo_categorie' => '/categorieLogo/pythonIcone.png',
        ]);

        DB::table('categories')->insert([
            'titre_categorie' => 'Langage-C',
            'description_categorie' => 'Vos question pour le langage C.',
            'logo_categorie' => '/categorieLogo/cIcone.png',
        ]);
        
        DB::table('categories')->insert([
            'titre_categorie' => 'Autres-Langages',
            'description_categorie' => 'Vous programmez dans un autre langage ? C\'est ici !',
            'logo_categorie' => '/categorieLogo/autreLangageIcone.png',
        ]);

        DB::table('categories')->insert([
            'titre_categorie' => 'Base-de-Données',
            'description_categorie' => 'Un souci avec Windows ? Une solution.',
            'logo_categorie' => '/categorieLogo/bddIcone.png',
        ]);


        DB::table('categories')->insert([
            'titre_categorie' => 'Windows',
            'description_categorie' => 'Une question à propos de Javascript.',
            'logo_categorie' => '/categorieLogo/windowsIcone.png',
        ]);

        DB::table('categories')->insert([
            'titre_categorie' => 'Linux',
            'description_categorie' => 'Vous avez un problèmes avec Linux ?',
            'logo_categorie' => '/categorieLogo/linuxIcone.png',
        ]);

        DB::table('categories')->insert([
            'titre_categorie' => 'Autres',
            'description_categorie' => 'D\'autres soucis ? Venez posez votre question ici.',
            'logo_categorie' => '/categorieLogo/autreIcone.png',
        ]);
    }
}
